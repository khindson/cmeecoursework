#!/usr/bin/Rscript
#Author: Katie Hindson kah15@ic.ac.uk
#Script: apply1.R
#Desc: demonstrating the use of apply
#Date: Oct 2016

## apply: applying the same function to rows/columns of a matrix

## Build a random matrix 
M <- matrix(rnorm(100), 10, 10)

## Take the mean of each row
RowMeans <- apply(M, 1, mean) # applying, to the rows, ***apply uses vectorization****
# a mean calculation of the entries in each row
print (RowMeans)

## Now the variance 
RowVars <- apply(M, 1, var) # calculating the varaince for each row...the (_, 1, _) tells it to use the row 
print (RowVars)

## By column
ColMeans <- apply(M, 2, mean) # clculte mean, but with columns
print (ColMeans)
