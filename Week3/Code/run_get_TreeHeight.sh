#!/bin/bash
#Author: Katie Hindson kah15@ic.ac.uk
#Script: run_get_tree_height.sh
#Desc: tests the get_TreeHeight R program using the default input trees.csv
#Arguments: none
#Date: Oct 2016

filename="../Data/trees.csv" # default filename to test the program

Rscript get_TreeHeight.R $filename 

