#!/usr/bin/Rscript
#Author: Katie Hindson kah15@ic.ac.uk
#Script: get_TreeHeight.R
#Desc: This function calculates heights of trees from the angle of 
#		elevation and the distance from the base using the trigonometric 
#		formula height = distance * tan(radians) and takes explicit input from the user to generate the results
#Args: degrees (The angle of elevation), distance (The distance from base)
#Output: The height of the tree, same units as "distance"
#Date: Oct 2016

args <- commandArgs(trailingOnly = TRUE) # with trailingOnly = T, commandArgs will return any arguments 
# explicitly passed by the user at the command line following the path to the R script

TreeHeight <- function(degrees, distance)
{
  radians <- degrees * pi / 180 # writing a conversion variable that changes degrees to radians
  height <- distance * tan(radians) # calculating height of the trees using trig equations
  #print(paste("Tree height is: ", height))
  return (height) # return the value of height
}

TreeHts <- read.csv(args[1], header = TRUE) # import with headers

library(tools)

stripped_name = basename(file_path_sans_ext(args[1])) # file_pth_sans_ext will extract the path up to, but not including the extension
# the basename function will get rid of the path information

filepath = paste('../Results/', stripped_name, '_treeheights.csv', sep = "")

Height = TreeHeight(TreeHts[,3], TreeHts[,2])

TreeHts$Tree.Height.m <- Height # adds a new column called Height.m to the TreeHts data frame and adds the values from 'Height' to this column

write.csv(TreeHts, filepath) # write it out as a new file



