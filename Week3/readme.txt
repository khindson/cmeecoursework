				    Week3

This directory contains the Code, Data and Sandbox sub-directories for 
week 3 (R Week). 

Code
----

Contains all of the code files written to be evaluated during Week3.

Data
----

Contains all of the data files used as needed for running code scripts. 

Sandbox
-------

Contains files used for experimenting either with output or scripts. Not 
to be mistaken for important and useful scripting masterpieces.

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

See below for the list and brief descriptions of each file contained in 
the sub-directories ordered alphabetically.

List of contents in the Code sub-directory:
==========================================

apply1.R
---------

Demonstrating the use of the apply function. 

apply2.R
---------

Another short code demonstrating the use of the apply function on matrices. 

basic_io.R
-----------

A simple R script to illustrate R input-output.

boilerplate.R
--------------

Simple boilerplate function in R deminstrating argument types. 

break.R
--------

Short code demonstrating the use of 'break' in loops.

browse.R
---------

Demonstrating using the browser() debugger tool in R on a simulation for 
exponential growth. 

control.R
----------

Short script demonstrating the use of different loop flavours in R. 

DataWrang.R
------------

Demonstration of data wrangling using the Pound Hill data collected. 

get_TreeHeight.R
------------------

This function calculates heights of trees from the angle of elevation and
the distance from the base using the trigonometric formula height = distance 
* tan(radians) and takes explicit input from the user in the form of a .csv
filepath and generates a new .csv file in the Results folder with the heights
calculations included. 

next.R
-------

Demonstrates the use of 'next' in a loop.

PP_Lattice.R
-------------

Takes data as input from the EcolArchives paper and prints three lattice graphs: 
	1)Predator mass by type of feeding interaction
	2)Prey mass by type of feeding interaction
	3)Prey mass/Predator mass by type of feeding interaction. 

PP_Regress.R
-------------

Makes a graph of prey mass vs. predator mass which is faceted by type of feeding 
interaction and then linear regressions of each predator lifestage are plotted 
on the graph in different colours in each of the facets. 

These linear regression results are then calculated and exported to a 
corresponding .csv file containing a summary of the statistcal analysis. 

run_get_TreeHeight.sh
----------------------

A bash script that runs the get_TreeHeight.R script using a default file input.

sample.R
---------

Runs a simulation that involves sampling from a population.

stochrickvect.R
---------------

Runs a vectorized version of the stochastic (with gaussian fluctuations) 
Ricker Eqn.

TAutoCorr.R
-----------

Runs an autocorrelation statistical test on the data set of Key West Florida's 
Annual Mean Temperature.

TAutoCorr_Results.tex
---------------------

A LaTeX file that describes the results from the TAutoCorr.R script. 

TreeHeight.R
------------

This function calculates heights of trees from the angle of elevation and
the distance from the base using the trigonometric formula height = distance 
* tan(radians). It outputs the results to a .csv file called 'TreeHeights.csv.'

try.R
-----

Short script that runs a simulation that involves sampling from a population 
using 'try.'

Vectorize1.R
------------

Demonstrating how much faster vectorization is compared to looping.

Vectorize2.R
------------

Original code that was edited to make a faster, vectorized version of the 
script (stochrickvect.R). 

List of contents in the Data sub-directory:
===========================================

EcolArchives-E089-51-D1.csv
---------------------------

Values for a bunch of species, their locations, mass, etc. all compiled in 
a .csv file.

KeyWestAnnualMeanTemperature.RData
----------------------------------

A data frame containing the mean annual temperature data from Key West, 
Florida from the years 1901 to 2000.

PoundHillData.csv
-----------------

Wrangled Pound Hill data collected by students. 

PoundHillMetaData.csv
---------------------

Raw data that was then wrangled to be used for further analyses. 

Results.txt
-----------

Random sampling of a distribution data used in demonstrations during week 3.

trees.csv
----------

A list of trees, the angle from which they were measured, and the distance
from which they were measured. This was used to calculate the height of the
trees in some scripts. 

List of contents in the Results sub-directory:
==============================================

Girko.pdf
---------

Random data points plotted on a real and uniform axis. 

MyBars.pdf
----------

A demonstration of overlaying bar graph data on a single plot. 

MyData.csv
----------

Demonstrating how to save a data frame within R...this is the trees.csv data.

MyFirst-ggplot2-Figure.pdf
--------------------------

Results from demonstrating how to do a ggplot of relation between predator and 
prey mass. 

MyLinReg.pdf
------------

Demonstrating how to perform a linear regression in R. 

PP_Regress_Results.csv
-----------------------

The output results file of the statistical summaries for the regressions 
on predator vs. prey mass faceted by feeding interaction type and further filtered
by predator life stage. 

PP_Results.csv
---------------

The output results file of some statistical measures on predator mass, prey mass, and 
predator/prey mass data factored by feeding interaction type. 

Pred_Lattice.pdf
----------------

A lattice graph of Predator mass by feeding interaction type.

Pred-Prey_Lifestage.Feeding.pdf
-------------------------------

The results from the PP_Regress file outputting the prey mass vs. predator mass
separted by feeding interaction types and then preadtor lifestage. 

Pred_Prey_Overlay.pdf
----------------------

Showing an overlay of bar graph data on the same pdf. 

Prey_Lattice.pdf
----------------

A lattice graph of Prey mass by feeding interaction type. 

SizeRatio_Lattice.pdf
---------------------

A lattice graph of predator mass/prey mass by feeding interaction type. 

TAutoCorr_Results.pdf
---------------------

The output .pdf file from the LaTeX file giving the autocorrelation results 
of the temperature data and their description. 

TreeHts.csv
-----------

The output file of running TreeHeights.R on trees.csv including the height
calculations in a new column. 

trees_treeheights.csv
----------------------

The output file if running get_TreeHeights.R using the default input trees.csv. 


List of contents in the Sandbox sub-directory:
==============================================

This is my coding Sandbox. In it I play, build my forts and castles, spend 
glorious time...alone...with my computer...and my rubber duck. 

So, you don't need to understand what goes on in here. 

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Contacts
--------

All script contents of this directory have been written by Katie Hindson or 
have been adapted from the CMEE 2016/17 Course Handbook written by Dr. Samraat
Pawar. 

e-mail questions (but obviously Google them first) to: kah15@ic.ac.uk



 




	



