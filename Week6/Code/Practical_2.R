#!/usr/bin/Rscript
#Author: Katie Hindson kah15@ic.ac.uk
#Script: Practical_2.R
#Desc: Practical 2 from genomics week
#Date: Nov 2016

rm(list=ls())

library(dplyr)
library(ggplot2)
library(reshape2)

g <- read.table("../Data/H938_chr15.geno", header=TRUE)
g<- mutate(g, nObs = nA1A1 + nA1A2 + nA2A2)

pdf("../Results/plot1.pdf")
print(qplot(nObs, data = g))
dev.off()

#computing genotype frequencies
g <- mutate(g, p11 = nA1A1/nObs, p12 = nA1A2/nObs, p22 = nA2A2/nObs)

#computing allele frequencies from genotype frequencies
g <- mutate(g, p1 = p11 + 0.5*p12, p2 = p22 + 0.5*p12)

pdf("../Results/plot2.pdf")
print(qplot(p1, p2, data=g))
dev.off()

gTidy <- select(g, c(p1,p11,p12,p22)) %>% melt(id='p1',value.name="Genotype.Proportion")

pdf("../Results/plot3.pdf")
print(ggplot(gTidy) + geom_point(aes(x = p1,
                               y = Genotype.Proportion,
                               color = variable,
                               shape = variable)))
dev.off()

pdf("../Results/plot4.pdf")
print(ggplot(gTidy) + geom_point(aes(x = p1, 
                               y = Genotype.Proportion,
                               color = variable,
                               shape = variable)) +
  stat_function(fun=function(p) p^2, geom="line", colour="red",size=2.5) +
  stat_function(fun=function(p) 2*p*(1-p), geom="line", colour="green", size=2.5) +
  stat_function(fun=function(p) (1-p)^2, geom="line", colour="blue", size=2.5))
dev.off()

g <- mutate(g, X2 = (nA1A1-nObs*p1^2)^2 /(nObs*p1^2) + 
              (nA1A2-nObs*2*p1*p2)^2 / (nObs*2*p1*p2) + 
              (nA2A2-nObs*p2^2)^2 / (nObs*p2^2)) 

g <- mutate(g,pval = 1-pchisq(X2,1))
sum(g$pval < 0.05, na.rm = TRUE)

pdf("../Results/plot5.pdf")
print(qplot(pval, data = g))
dev.off()

pdf("../Results/plot6.pdf")
print(qplot(2*p1*(1-p1), p12, data = g) + geom_abline(intercept = 0, slope=1, color="red", size=1.5))
dev.off()

pDefHet <- mean((2*g$p1*(1-g$p1)-g$p12) / (2*g$p1*(1-g$p1)))

g <- mutate(g, F = (2*p1*(1-p1)-p12) / (2*p1*(1-p1)))

pdf("../Results/plot7.pdf")
print(plot(g$F, xlab = "SNP number"))
dev.off()

movingavg <- function(x, n=5){stats::filter(x, rep(1/n,n), sides = 2)} 

pdf("../Results/plot8.pdf")
print(plot(movingavg(g$F), xlab="SNP number"))
dev.off()


outlier=which (movingavg(g$F) == max(movingavg(g$F), na.rm=TRUE)) 
g[outlier,]