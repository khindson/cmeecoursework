					Week6

This directory contains the Code, Data, Sandbox, Handouts, and Lectures 
sub-directories for week 6 (genomics).   

Code
----

Contains all of the code files written to be evaluated during Week6.

Data
----

Contains all of the data files used as needed for running code scripts. 

Sandbox
-------

Contains the practicals and lectures for the week.

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

See below for the list and brief descriptions of each file contained in 
the sub-directories ordered alphabetically.

List of contents in the Code sub-directory:
==========================================

Practical_2.R
-------------

Practical 2 from genomics week looking at basic population genomics using R. 

List of contents in the Data sub-directory:
==========================================

Empty. 

List of contents in the Results sub-directory:
=============================================

plot1.pdf
---------

Histogram of the observations. 

plot2.pdf
---------

Plot of the frequency of the major allele vs the frequency of the minor 
allele. 

plot3.pdf
---------

Plot of the proportion of the three genotypes in the dataset.

plot4.pdf
---------

A plot with lines that represent Hardy-Weinberg proportions of the three
genotypes.  

plot5.pdf
---------

A plot of the p-values distributions for the data. 

plot6.pdf
---------

A plot of the expected vs. observed heterozygosity.

plot7.pdf
---------

A plot of the SNP number vs. the genotype frequency. 

plot8.pdf
---------

Final plot of the population genomics practical. 

List of contents in the Sandbox sub-directory:
==============================================

Lectures and practical files for the week are located in this sub-directory.

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Contacts
--------

All script contents of this directory have been written by Katie Hindson or 
have been adapted from the CMEE 2016/17 Course Handbook written by Dr. Samraat
Pawar. 

e-mail questions (but obviously Google them first) to: kah15@ic.ac.uk










