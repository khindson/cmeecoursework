CMEE 2016-17 Coursework Repository

Materials separated into weekly directories:

Week1 : Introduction to UNIX and Linux
	Advanced UNIX: Shell scripting
	Version control with Git
	Using LaTeX for Scientific Documents

Week2 : Basic Biological Computing in Python

Week3:  Introduction to R
	Data wrangling, exploration, and visualization in R
	Advanced Topics in R

Week4: 	Basic Statistics (no scripts for this week)

Week 5: GIS 

Week 6: Genomics 

Week 7: Advanced Biological Computing in Python

Week 9: High Performance Computing

cmeeminiproject: Project based on performing model selection and 
		 analyzing possible effect of acclimation in FLUXNET 
		 respiration data.

