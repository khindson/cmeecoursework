				    Week1

This directory contains the Code, Data and Sandbox sub-directories for 
week 1. 

Code
----

Contains all of the code files written to be evaluated during Week1.

Data
----

Contains all of the data files used as needed for running code scripts. 

Sandbox
-------

Contains files used for experimenting either with output or scripts. Not 
to be mistaken for important and useful scripting masterpieces.

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

See below for the list and brief descriptions of each file contained in 
the sub-directories ordered alphabetically.

List of contents in the Code sub-directory:
==========================================

boilerplate.sh
--------------

A simple boilerplate skeleton for shell scripts. Demonstrates shebang and 
conventional initial code description. 

CompileLaTeX.sh
---------------

A LaTeX compiling program that first removes the file extension (if any) 
of the input argument filename then compiles it to a pdf file and its 
corresponding bibliography. 

NOTE: if inputting a filename that has a '.' character but no extension, 
the code will remove the characters after the '.' and the program won't 
work as designed. Ex. filename.test == filename


ConcatenateTwoFiles.sh
----------------------

Takes three files as arguments and merges the first two arguments to 
become/replace the contents of the third. 

NOTE: The third argument should be an empty document, otherwise its 
contents will be rewritten.

CountLines.sh
-------------

Takes a file as an argument and counts the number of lines in that 
file and displays the results.

csvtospace.sh
-------------

Takes a csv file as an argument, removes the commas from the file, 
replaces them with spaces and outputs the resulting file as a .txt 
file with the same name. 

FirstBiblio.bib
---------------

LaTeX bibliography file corresponding to the FirstExample.tex.

FirstExample.tex
----------------

LaTeX skeleton file for the generic and most basic construction of a 
LaTeX written research paper. 

FirstExample.bbl, FirstExample.blg, FirstExample.pdf
----------------------------------------------------

All output files from running CompileLaTeX.sh on FirstExample.tex.

The first is a bibliography compilation, the second is a binary 
performance log file and the third is the document compiled into a pdf 
format. 

MyExampleScript.sh
------------------

Short code demonstrating the variable assignment in bash and the default
value of $USER.

tabtocsv.sh
-----------

Takes a file argument and creates a comma delimited version of it by 
replacing any series of tabs with single commas. Outputs a file with the 
same name and .csv extension containing the comma delimited version.

UnixPrac1.txt
-------------

Code used for reading and interpreting aspects of .fasta files. Takes 
.fasta file arguments. The code is composed of 5 parts:
	1) Counting the number of lines in the argument files
	2) Printing the genome in an argument file (not including 
	   the header)
	3) Counting the number of characters in a genome (again, 
	   not including the header)
	4) Counting the matches of the sequence, 'ATGC' in a genome
	5) Calculating the AT/GC ratio in a genome

variables.sh
------------

Code to illustrate how to assign values to and the use of variables in 
some basic commands.

List of contents in the Data sub-directory:
===========================================

FASTAfiles
----------

Genome sequence data used to test and run the UnixPrac1 bash script. 
Contains 3 fasta files with three genome sequences and two files 
separtely containing the number of AT and GC occurrences in the E.coli
genome. 

Temperatures
------------

Contains both .csv and .txt files with data describing the max temperature,
min temperature and total precipitation in different locations. This data 
was used for the csvtospace.sh script as input for testing. 

List of contents in the Sandbox sub-directory:
==============================================

This is my coding Sandbox. In it I play, build my forts and castles, spend 
glorious time...alone...with my computer...and my rubber duck. 

So, you don't need to understand what goes on in here. 

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Contacts
--------

All script contents of this directory have been written by Katie Hindson or 
have been adapted from the CMEE 2016/17 Course Handbook written by Dr. Samraat
Pawar. 

e-mail questions (but obviously Google them first) to: kah15@ic.ac.uk



 




	



