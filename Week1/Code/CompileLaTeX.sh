#!/bin/bash

FileName=$1
a="${FileName%.*}" 

##${} is parameter expansion...the %PATTERN will try to match the pattern starting reading from the end of the string
##in this case our 'pattern' is the '.*' which should be wildcard characters (the extension) found before a '.'
##This should remove the extension if there is one

pdflatex $a.tex #pdflatex is one way of compiling a latex file to a pdf
pdflatex $a.tex #it's not very effective at compiling, so you need to compile twice
bibtex $a
pdflatex $a.tex #again, once you've compiled the bibtex, you need to compile the latex file again twice
pdflatex $a.tex
evince $a.pdf &

##Cleanup
rm *~
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc

