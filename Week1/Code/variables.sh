#!bin/bash
#Shows the use of variables
MyVar='random words'
echo 'the current value of the variable is' $MyVar
echo 'Please enter a new string'
read MyVar #Taking user input to rewrite over MyVar
echo 'the current value of the variable is' $MyVar
## Reading multiple values
echo 'Enter two numbers separated by space (s)'
read a b
echo  'you entered' $a 'and' $b '. Their sum is: '
mysum=`expr $a + $b`
echo $mysum

