#!/bin/bash
# Author: Katie Hindson kah15@ic.ac.uk
# Script: UnixPrac1.txt
# Desc: changing a csv and converting it to a space separated file
# Arguments: $1
# Date: Oct 2016

echo "Creating a space separated version of $1..."

cat $1 | tr -s "," " " >> $1.txt
#this replaces the commas in the .csv file with spaces and saves it to a new .txt file
#the -s command means that if there is more than one of the first character in a row, 
#it will replace the entire grouping of characters with a single one of the second replacement characters
#for example, ',,,,,,' would be replaced as ' ' rather than '      '.


echo "Done!"

exit
