				    Week5

This directory contains the Code, Data, Sandbox, Handouts, and Lectures sub-directories for 
week 5 (GIS).   

Code
----

Contains all of the code files written to be evaluated during Week3.

Data
----

Contains all of the data files used as needed for running code scripts. 

Sandbox
-------

Contains the practicals and lectures for the week.

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

See below for the list and brief descriptions of each file contained in 
the sub-directories ordered alphabetically.

List of contents in the Code sub-directory:
==========================================

clifford.test.R
---------------

Statistical test used for the Spatial Modelling practical. 

script_prac1.py
---------------

Script used to automate merging rasters, format conversion and zonal 
statistics. Takes in files and merges them and then performs zonal statistics
and gives an output. 

Spatial_Modelling_Practical.R
-----------------------------

All of the code run and used during the spatial modelling practical.

List of contents in the Data sub-directory:
===========================================

EU
--

Data used for automating the running of the zonal statistics practical. 
Contains spatial information across the UK including raster data and other panels. 

Spat_Mod_Data
-------------

Avian richness data in Africa used in the spatial modelling practical. 

List of contents in the Results sub-directory:
==============================================

Maxent_Outputs
--------------

Resulting output files from the Maxent practical.

bio1_UK_BNG.tif
----------------

The results of the UK climatic data for bio1 data type warped into the 
British National Grid view. 

bio12_UK_BNG.tif
----------------

The results of the UK climatic data for bio12 data type warped into the 
British National Grid view. 

zonalstats.csv
--------------

This is the zonal statistics file containing the information from zonal 
statistics run on the two climatic variable types, bio1 and bio12, on
the UK land cover data (bio1_UK_BNG.tif and bio12_UK_BNG.tif). 

List of contents in the Sandbox sub-directory:
==============================================

Lectures and practical files for the week are located in this sub-directory.

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Contacts
--------

All script contents of this directory have been written by Katie Hindson or 
have been adapted from the CMEE 2016/17 Course Handbook written by Dr. Samraat
Pawar. 

e-mail questions (but obviously Google them first) to: kah15@ic.ac.uk



 




	



