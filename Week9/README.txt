				    Week7

This directory contains the Code, Results and written answers for 
week 9 (High Performance Computing). 

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 

See below for the list and brief descriptions of each file contained in 
the HPC/Week9 folder. 

kah16.R
--------

Contains the R code for every question in the HPC week. Is made up of a set 
of functions and is not self-running. The functions will need to be called 
in order to be evaluated.
Further descriptions of each function are clearly written as comments in the 
code.
 
kah16_cluster_run_test.R
-------------------------

This is R code used to test the neutral simulation code to be run on the HPC. 
It uses a single input as the iteration # and only runs for a short period of time
(as specified in the code). 

kah16.sh
---------

The shell script used to run a neutral model simulation on the HPC. Gives a 
run time and produces printed output so that it can eb established when/if the 
code has started running and when/if it finished normally. 

kah16_cluster.zip
=================

Files within kah16_cluster.zip are as follows:

kah16_cluster_run_hpc.R
------------------------

The R code that was used to run the neutral simulation on the HPC. This code is 
self-running, so the line used to call the neutral simulation function is appended 
to the end of the code (this is the only difference between this code and the 
corresponding function code in kah16.R).

Results
=======
NeutralSim_#(1-100).rda
------------------------

These are the resulting .rda files from running the neutral simulation on the HPC.
Each file contains the time series (a vector of species richness values across the 
time series AFTER the burn-in period), octave_list (a list of the octave vectors at 
each time step), sim_start (the final community structure of the simulation), timer
(the amount of time that passed before the simulation ended), speciation_rate, size, 
wall_time (the time at which the simulation stops running and starts saving results), 
rand_seed (the random seed number assigned to the simulation), interval_rich (the 
interval at which the species richness is stored during the simulation), interval_oct 
(the interval at which the species abundance octaves were calculated during the 
simulation), burn_in_time (the amount of generations run before the species richness
starts being stored/calculated).  

TestResults
-----------

This is the folder containing the .rda file results from the test run of the neutral 
simulation model. These files can be ignored. 


>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Contacts
--------

All script contents of this directory have been written by Katie Hindson.

e-mail questions to: kah15@ic.ac.uk
 







