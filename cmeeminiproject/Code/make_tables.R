#!/usr/bin/Rscript
#Author: Katie Hindson kah15@ic.ac.uk
#Script: make_tables.R
#Desc: This builds the tables to be added to the LaTeX file at the end 
#      NOTE: you are required to have installed the xtable package as this
#            is used for building the tables
#Output: Table_1.jpg and Table_2.jpg
#Date: Nov 2016

rm(list=ls())

require(xtable)
require(ggplot2)

##################################################################################
############################ MAKE_TABLE FUNCTION #################################
##################################################################################

make_table <- function(file){
  
  load(file) # loading the ecosystem model results file into the global environment
  
  # function that specifies to show/round to only 2 digits after the decimal point
  specify_decimal <- function(x) {
    number <- as.numeric(format(round(x, 2), nsmall=2))
    return(number)
  }
  
  Quad_proportion_day <- specify_decimal(model_fit_counter_day[1]/sum(model_fit_counter_day)) # proportion of Quad as a better fit in the day data
  sig_better_fit_Quad_day <- paste0(specify_decimal(day_model_results[1]/sum(day_model_results))*100, "%") # percentage of those Quad as a better fit 
                                                                                                           # that were significantly better fit than the 
                                                                                                           # Boltzmann-Arrhenius
  
  Quad_proportion_week <- specify_decimal(model_fit_counter_week[1]/sum(model_fit_counter_week)) # proportion of Quad as a better fit in the week data
  sig_better_fit_Quad_week <- paste0(specify_decimal(week_model_results[1]/sum(week_model_results))*100, "%") # percentage of those Quad as a better fit 
                                                                                                              # that were significantly better fit than the 
                                                                                                              # Boltzmann-Arrhenius
  
  Quad_proportion_month <- specify_decimal(model_fit_counter_month[1]/sum(model_fit_counter_month)) # proportion of Quad as a better fit in the month data
  sig_better_fit_Quad_month <- paste0(specify_decimal(month_model_results[1]/sum(month_model_results))*100, "%") # percentage of those Quad as a better fit 
                                                                                                                 # that were significantly better fit than the 
                                                                                                                 # Boltzmann-Arrhenius
  X_2 <- as.numeric(chisq.test(tbl)[[1]]) # the chi squared statistic value for the chisquared test for independence of 
                                          # temporal scale vs. model of best fit selection
  df <- format(round((as.numeric(chisq.test(tbl)[[2]])), 0), nsmall = 0) # the degrees of freedom for the test
  p_value <- as.numeric(chisq.test(tbl)[[3]]) # the p-value result from the test
  N <-  format(round((sum(tbl)), 0), nsmall = 0) # the sample size used in the test
  
  return(list(Quad_proportion_day, sig_better_fit_Quad_day, Quad_proportion_week, 
              sig_better_fit_Quad_week,  Quad_proportion_month,
              sig_better_fit_Quad_month, X_2, df, p_value, N))
  
  
}

##################################################################################
########### CALLING THE MAKE_TABLE FUNCTION AND BUILDING THE PLOTS ###############
##################################################################################

# these are all of the ecosystem types with available results from NLLS
ecosystem_types <- c("DBF", "SAV", "GRA", "ENF", "OSH", "CRO", "WET", "WSA", "MF", "CSH", "EBF")

# making empty vectors that are then going to be coerced into a data frame and made into tables
BA_fits_day <- rep(NA, length(ecosystem_types))
Quad_fits_day <- rep(NA, length(ecosystem_types))
sig_better_day <- rep(NA, length(ecosystem_types))
BA_fits_week <- rep(NA, length(ecosystem_types))
Quad_fits_week <- rep(NA, length(ecosystem_types))
sig_better_week <- rep(NA, length(ecosystem_types))
BA_fits_month <- rep(NA, length(ecosystem_types))
Quad_fits_month <- rep(NA, length(ecosystem_types))
sig_better_month <- rep(NA, length(ecosystem_types))

chisq_vals <- rep(NA, length(ecosystem_types))
dfs <- rep(NA, length(ecosystem_types))
p_vals <- rep(NA, length(ecosystem_types))
sample_size <- rep(NA, length(ecosystem_types))

for (i in 1:length(ecosystem_types)){
  # the file name for the given ecosystem type
  FileName <- Sys.glob(paste0("../Results/Model_Selection_vals_actual/", ecosystem_types[i], "*"))
  # run make_table on the given ecosystem type's results
  table_vals <- make_table(FileName)
  
  Quad_fits_day[i] <- table_vals[[1]]
  sig_better_day[i] <- table_vals[[2]]
  Quad_fits_week[i] <- table_vals[[3]]
  sig_better_week[i] <- table_vals[[4]]
  Quad_fits_month[i] <- table_vals[[5]]
  sig_better_month[i] <- table_vals[[6]]
  
  chisq_vals[i] <- table_vals[[7]]
  dfs[i] <- table_vals[[8]]
  p_vals[i] <- table_vals[[9]]
  sample_size <- table_vals[[10]]
}

##### Building the data frame and table for the Model Fits table #####

# taking the proportion of Boltzmann-Arrhenius best fits as 1 - the proportion of quadratic best fits
Boltz_fits_day <- 1 - Quad_fits_day 
Boltz_fits_week <- 1- Quad_fits_week
Boltz_fits_month <- 1 - Quad_fits_month

# making vectors that are factors for the day, week and month time scales so that I can
# separate the plot by these temporal factors after
Fits_time <- factor(c((rep("D", 11)), (rep("W", 11)), (rep("M", 11)), (rep("D", 11)), (rep("W", 11)), (rep("M", 11))))
ecosystem_types_new <- rep(ecosystem_types, 6)
Fits_vals <- c(Quad_fits_day, Quad_fits_week, Quad_fits_month, Boltz_fits_day, Boltz_fits_week, Boltz_fits_month)
Quad_or_Boltz <- factor(c((rep("Second-degree polynomial", 33)), (rep("Boltzmann-Arrhenius", 33))))

model_fits_df <- cbind.data.frame(ecosystem_types_new, Fits_time, Fits_vals, Quad_or_Boltz)

pdf("../Report/Figures/model_fits_table.pdf", width = 10, height = 6)
plot <- ggplot(model_fits_df[order(model_fits_df$Quad_or_Boltz, decreasing = F),], 
               # the above bit just puts the Boltzmann-arrhenius proportion on the bottom of the plot instead of the top
               aes(x = Fits_time, y = Fits_vals, fill = Quad_or_Boltz)) + 
  geom_bar(stat = 'identity') + 
  facet_grid(~ecosystem_types_new) + 
  xlab("Temporal Scale (D = Day, W = Week, M = Month)") + 
  ylab("Proportion of Best Fits") +
  ggtitle("Proportion of second-degree polynomial vs. Boltzmann-Arrhenius \nbest fits for each ecosystem type at each time scale") + 
  labs(fill = "Model selected")
print(plot)
dev.off()

##### Building the data frame and table for the Chi-Squared Results table #####

chisq_df <- cbind.data.frame(ecosystem_types, dfs, sample_size, chisq_vals,  p_vals)
colnames(chisq_df) <- c("Ecosystem Type", "Degrees of Freedom", "Sample Size (N)", "Chi-Squared", "p-value")
# saving the table as LaTeX code in the Report directory
print(xtable(chisq_df), include.rownames = FALSE, file = paste0("../Report/Figures/chisq_table.txt"))



