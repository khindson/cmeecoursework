#!/usr/bin/Rscript
#Author: Katie Hindson kah15@ic.ac.uk
#Script: split_night_day.R
#Desc: This parses & subsets the data into nighttime data and daytime data. 
#Date: Nov 2016
#Output: FitDataNightHH_clean.Rdata and FitDataDayHH_clean.Rdata

rm(list=ls()) # clear objects
library(plyr)

#load("../Data/fluxnet_clean_ecoK.Rdata")
load("../Data/sample_data_clean_ecoK.Rdata")
finalfluxnet <- fluxnet_clean_ecoK  

################################################################################
######### Subsetting the data into nighttime and daytime observations ##########
################################################################################


############ screened nighttime fluxnet data ############
#########################################################

# Take the nighttime data as data for which there is no photosynthesis...only respiration
# ...there may be data points that have a negative NEE because night and day are not 
# binary things...there is a transition period for which there could be some light and
# photosynthesis occurring...thus giving these negative values. 
FitDataNightHH <- finalfluxnet[which(finalfluxnet$NIGHT == 1 & finalfluxnet$NEE_VUT_REF > 0),]
#FitDataNightHH <- subset_df[which(subset_df$NIGHT == 1 & subset_df$NEE_VUT_REF > 0),]

#remove years with <=100 points
tbl     <- table(FitDataNightHH$sitebyyear)                      #make table of the data subsetted by the site by year
FitDataNightHH <- FitDataNightHH[FitDataNightHH$sitebyyear %in%  #match the names of that subset that appear more than 100 times
                     names(tbl)[tbl > 100],, drop=FALSE] 
FitDataNightHH <- droplevels(FitDataNightHH)                     #droplevels drops the factor levels in the subsetted 
                                                                 #dataframe that have a 0 occurrence

#getting rid of sitebyyear which have a temperature range of less than 5 degrees 
#getting the temperature ranges for each sitebyyear factor value
temp_ranges <- c(by(FitDataNightHH, FitDataNightHH$sitebyyear, function(x){
  #caculate the temperature range for each sitebyyear in fluxnet
  range_temp <- max(x$TA_F) - min(x$TA_F)
}))
sitebyyear <- unique(FitDataNightHH$sitebyyear)
#making a data frame of the temperature ranges vector and the fluxnet sitebyyear list
ranges_years <- data.frame(sitebyyear = sitebyyear, temp_ranges = temp_ranges)
# for some reason, saves sitebyyear as a factor, so this is converting the factor in 
# the data frame back to a character, then re-setting it to the sitebyyear values
#...it works, but there's probably a better way...
ranges_years[1] <- "character"
ranges_years[1] <- sitebyyear
#subsetting the data frame to only include the sitebyyears which had a temperature range >= 5
ranges_greaterthan5 <- ranges_years[which(ranges_years$temp_ranges >= 5),]
#removing the sites from the fluxnet data that were not in the subset of values with a temperature
#range >= 5
FitDataNightHH <- FitDataNightHH[FitDataNightHH$sitebyyear %in% c(ranges_greaterthan5$sitebyyear),]

# rename the data frame and save it 
night_clean <- FitDataNightHH
#save(night_clean, file = "../Data/FitDataNightHH.Rdata") 
save(night_clean, file = "../Data/sample_DataNight.Rdata")

############ screened daytime fluxnet data ############
#######################################################

FitDataDayHH <- subset(finalfluxnet, NIGHT == 0)
#FitDataDayHH <- subset(subset_df, NIGHT == 0)

#remove years with <=100 points
tbl     <- table(FitDataDayHH$sitebyyear)                      #make table of the data subsetted by the site by year
FitDataDayHH <- FitDataDayHH[FitDataDayHH$sitebyyear %in%  #match the names of that subset that appear more than 100 times
                                   names(tbl)[tbl > 100],, drop=FALSE] 
FitDataDayHH <- droplevels(FitDataDayHH)                     #droplevels drops the factor levels in the subsetted 
#dataframe that have a 0 occurrence

#getting rid of sitebyyear which have a temperature range of less than 5 degrees 
#getting the temperature ranges for each sitebyyear factor value
temp_ranges <- c(by(FitDataDayHH, FitDataDayHH$sitebyyear, function(x){
  #caculate the temperature range for each sitebyyear in fluxnet
  range_temp <- max(x$TA_F) - min(x$TA_F)
}))
sitebyyear <- unique(FitDataDayHH$sitebyyear)
#making a data frame of the temperature ranges vector and the fluxnet sitebyyear list
ranges_years <- data.frame(sitebyyear = sitebyyear, temp_ranges = temp_ranges)
# for some reason, saves sitebyyear as a factor, so this is converting the factor in 
# the data frame back to a character, then re-setting it to the sitebyyear values
#...it works, but there's probably a better way...
ranges_years[1] <- "character"
ranges_years[1] <- sitebyyear
#subsetting the data frame to only include the sitebyyears which had a temperature range >= 5
ranges_greaterthan5 <- ranges_years[which(ranges_years$temp_ranges >= 5),]
#removing the sites from the fluxnet data that were not in the subset of values with a temperature
#range >= 5
FitDataDayHH <- FitDataDayHH[FitDataDayHH$sitebyyear %in% c(ranges_greaterthan5$sitebyyear),]

# rename the data frame and save it 
day_clean <- FitDataDayHH
#save(day_clean, file = "../Data/FitDataDayHH.Rdata") 
save(day_clean, file = "../Data/sample_DataDay.Rdata")

