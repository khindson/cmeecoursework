#!usr/bin/python

"""This runs my entire mini project on a set of sample data.
Note that the code would run on the full data set, but you can see this
option is hashed out in all of the code. 

The code flow is as follows: 
1. data_cleanup_simple.R 
2. ecosystemtype_added.R
3. split_night_day.R
4. subset_data.R
5. NLLS_do.R
6. extract_values_NLLS.R
7. graph_plots.R
8. make_tables.R 
9. CMEE_miniproject_KHindson_sample.tex
10. word_count_sample.sh
11. CMEE_miniproject_KHindson_sample.tex"""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import subprocess
import os
import shutil

# data_cleanup_simple.R cleans the raw data and saves the cleaned data as a new .Rdata file 
subprocess.Popen("Rscript --verbose data_cleanup_simple.R", shell=True).wait()

# ecosystemtype_added.R adds a column with the the ecosystem type IDs for each of 
# the entries in the cleaned data and another column for the temperature in Kelvin of 
# each entry then saves this to a new .Rdata file
subprocess.Popen("Rscript --verbose ecosystemtype_added.R", shell=True).wait()

# split_night_day.R splits the cleaned data with the new columns into night only 
# and day only subsets then saves these to new .Rdata files
subprocess.Popen("Rscript --verbose split_night_day.R", shell=True).wait()

# making directories to save the results files from subset_data.R
# removing the directories if they already exist and replacing them with new
# ones...this way, you don't try to analyze data that was previously in the 
# files but is no longer relevant to the study you're doing. 
if os.path.exists("../Data/SingleDayData"):
	shutil.rmtree("../Data/SingleDayData")
os.makedirs("../Data/SingleDayData")

if os.path.exists("../Data/SingleWeekData"):
	shutil.rmtree("../Data/SingleWeekData")
os.makedirs("../Data/SingleWeekData")

if os.path.exists("../Data/SingleMonthData"):
	shutil.rmtree("../Data/SingleMonthData")
os.makedirs("../Data/SingleMonthData")

if os.path.exists("../Data/SplitByEcotypes"):
	shutil.rmtree("../Data/SplitByEcotypes")
os.makedirs("../Data/SplitByEcotypes")

# subset_data.R subsets the data by ecosystem types, saves this as a new .Rdata file
# then uses this ecosystem-split data to then further split it into day, week and month
# observations subset by ecosystem type. It saves all of these as .Rdata files in their
# respective directories
subprocess.Popen("Rscript --verbose subset_data.R", shell=True).wait()

# making directories for the NLLS_do.R results files 
if os.path.exists("../Results/NLLS_Results"):
	shutil.rmtree("../Results/NLLS_Results")
os.makedirs("../Results/NLLS_Results")
if os.path.exists("../Results/NLLS_Errors"):
	shutil.rmtree("../Results/NLLS_Errors")
os.makedirs("../Results/NLLS_Errors")

# NLLS_do.R runs the NLLS on each of the day, week, and month split by ecosystem type
# files and then saves these results to the NLLS_Results directory as .rda files
subprocess.Popen("Rscript --verbose NLLS_do.R", shell=True).wait()

# making directories for the extract_values_NLLS.R results 
if os.path.exists("../Results/Extracted_NLLS_vals"):
	shutil.rmtree("../Results/Extracted_NLLS_vals")
os.makedirs("../Results/Extracted_NLLS_vals")

if os.path.exists("../Results/Model_Selection_vals"):
	shutil.rmtree("../Results/Model_Selection_vals")
os.makedirs("../Results/Model_Selection_vals")

# extract_values_NLLS.R extracts all of the NLLS fitted values, the best-fitting model
# data, runs chi-squared and ANOVA tests and saves all of the results for each ecosystem
# type's data
subprocess.Popen("Rscript --verbose extract_values_NLLS.R", shell=True).wait()

# making directories for the graph_plots.R results
if os.path.exists("../Results/Multiplots"):
	shutil.rmtree("../Results/Multiplots")
os.makedirs("../Results/Multiplots")

if os.path.exists("../Results/Multiplots_Failed"):
	shutil.rmtree("../Results/Multiplots_Failed")
os.makedirs("../Results/Multiplots_Failed")

# graph_plots.R makes multiplot graphs for sequential day, week and month data for
# each ecosystem type where there are corresponding day, week and month data sets 
# available. It saves the multiplots to the previously created directory.
subprocess.Popen("Rscript --verbose graph_plots.R", shell=True).wait()

# make_tables.R creates the chi-squared and model of best fit tables for the report
# in an exported LaTeX-readable file that is then used in the report. 
subprocess.Popen("Rscript --verbose make_tables.R", shell=True).wait()

# this is compiling the report and running the word count 
subprocess.Popen("../Report/CompileLaTeX_sample.sh").wait()
subprocess.Popen("../Report/word_count_sample.sh").wait()
subprocess.Popen("../Report/CompileLaTeX_sample.sh").wait()













