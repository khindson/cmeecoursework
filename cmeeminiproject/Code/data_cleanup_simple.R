#!/usr/bin/Rscript
#Author: Katie Hindson kah15@ic.ac.uk
#Script: siteID_added.R
#Desc: This is cleaning the data from the full data set to remove low quality 
#      observations, only growth period, and only sites with a number of data 
#      points above a certain threshold.
#Output: fluxnet_clean which is the cleaned full data set. 
#Date: Nov 2016

rm(list=ls())
library(plyr)
#load("../Data/FullDataSetHH.Rdata") # loading data into global environment
load("../Data/sample_data.Rdata") # loading sample data into the global environment

#fluxnet <- FullDataSetHH                                  #rename data set 
fluxnet <- sample_data

# removing the dashes in the siteid and the siteidbyyear columns...they complicate
# things in R because they can't be used as variable names if they have dashes in them
fluxnet$site.id <- gsub('-', '', fluxnet$site.id)
fluxnet$sitebyyear <- gsub('-', '', fluxnet$sitebyyear)

########################### screened fluxnet data ###########################
## this is the only thing I would change if making the clean up more complex...this isn't techincally
## the growth period if you just use teh temperatures that are freezing...
fluxnet <- fluxnet[fluxnet$TA_F > 0,]                     #remove freezing temps
fluxnet <- fluxnet[!(fluxnet$PPFD_IN <= 0 & fluxnet$NIGHT == 0),]                 #remove dark days
fluxnet <- fluxnet[fluxnet$GPP_NT_VUT_REF > 0.1,]         #remove anomolous low gpp
fluxnet <- fluxnet[fluxnet$RECO_NT_VUT_REF > 0.1,]        #remove anomolous low r
fluxnet <- fluxnet[fluxnet$NEE_VUT_REF_QC %in% c(0, 1) ,] #remove interpolated data
#remove years with <=100 points
tbl     <- table(fluxnet$sitebyyear)                      #make table of the data subsetted by the site by year
fluxnet <- fluxnet[fluxnet$sitebyyear %in%                #match the names of that subset that appear more than 100 times
                    names(tbl)[tbl > 100],, drop=FALSE] 
fluxnet <- droplevels(fluxnet)                            #droplevels drops the factor levels in the subsetted 
                                                          #dataframe that have a 0 occurrence
fluxnet[fluxnet==-9999.00]<-NA                            #replacing all of the -9999.00 entries with NAs

#getting rid of sitebyyear which have a temperature range of less than 5 degrees 
#getting the temperature ranges for each sitebyyear factor value
temp_ranges <- c(by(fluxnet, fluxnet$sitebyyear, function(x){
               #caculate the temperature range for each sitebyyear in fluxnet
               range_temp <- max(x$TA_F) - min(x$TA_F)
               }))
sitebyyear <- unique(fluxnet$sitebyyear)
#making a data frame of the temperature ranges vector and the fluxnet sitebyyear list
ranges_years <- data.frame(sitebyyear = sitebyyear, temp_ranges = temp_ranges)
# for some reason, saves sitebyyear as a factor, so this is converting the factor in 
# the data frame back to a character, then re-setting it to the sitebyyear values
#...it works, but there's probably a better way...
ranges_years[1] <- "character"
ranges_years[1] <- sitebyyear
#subsetting the data frame to only include the sitebyyears which had a temperature range >= 5
ranges_greaterthan5 <- ranges_years[which(ranges_years$temp_ranges >= 5),]
#removing the sites from the fluxnet data that were not in the subset of values with a temperature
#range >= 5
fluxnet <- fluxnet[fluxnet$sitebyyear %in% c(ranges_greaterthan5$sitebyyear),]

# rename the data frame and save it 
fluxnet_clean <- fluxnet
#save(fluxnet_clean, file = "../Data/fluxnet_clean.Rdata") 
save(fluxnet_clean, file = "../Data/sample_data_clean.Rdata")
