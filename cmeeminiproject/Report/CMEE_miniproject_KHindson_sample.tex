
\documentclass[a4paper,11.0pt,twoside]{article}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=3cm]{geometry}
%\renewcommand{\familydefault}{\sfdefault}
\usepackage{etoolbox,lineno} %package used for adding line numbers..etoolbox used for when we remove the section title line numbers
\usepackage{setspace} %this is setting the spacing to 1.5 lines
\doublespacing
\usepackage[round]{natbib}   % omit 'round' 
\bibliographystyle{plainnat}
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{ {Figures/} }
\usepackage{float}
\usepackage{titlesec}
\usepackage[justification=centering]{caption}
\usepackage{import}
\usepackage{afterpage}
% setting the title spacing to be left spacing 0pt, above spacing 6pt below spacing 6pt for both section and subsection titles
\titlespacing\section{0pt}{6pt}{6pt}
\titlespacing\subsection{0pt}{6pt}{6pt}

\begin{document}
	\begin{titlepage}
		\centering
		{\huge\bfseries Temperature dependence of plant metabolism: a time-sensitive trait? \par} 
		\vspace{2cm}
		{\Large By: \\ Katie Hindson\par}
		\vspace{2cm}
		{\Large\bfseries Department of Life Sciences \\ Imperial College London \\
			London SW7 2AZ \\ United Kingdom \par}
		\vspace{1.5cm}
		
		\begin{figure}[!h]
			\centering
			\includegraphics[scale=0.4]{Figures/IC_Crest.eps}
		\end{figure}
		\vfill
		
		% Bottom of the page
		{\large\bfseries CMEE Mini Project Submission\par}
		% getting the total wordcount from the calculated
		% wordcount file that uses texcount.pl to get an estimate
		Word Count: \input{miniproject_wordcount_sample.txt}
	\end{titlepage}
	
	%This is adding line numbers to the document starting after the title page...initializing adding line numbers
	\linenumbers
	
	\section{Introduction}
	%write the intro/background, project idea and proposed questions here
	
	There is widespread acceptance that climate change is rapidly and profoundly altering our natural environment (\citet{houghton2001climate}, \citet{cramer2001global}, \citet{frank2015effects}). The most recent projections predict a warming between 0.3 and 4.8 $^{\circ}$C by the end of the century (\citet{field2014ipcc}, \citet{stocker2014climate}) and the impact of this increase will be experienced across all scales of biological systems: from kinetics in metabolic reactions to ecosystem biogeochemical processes including carbon cycling (\citet{yuan2011thermal}). 
	
	Mechanistic mathematical models that successfully describe the temperature dependence of biological traits are incredibly valuable to studies predicting the effects of climate change on biochemical kinetics (\citet{huey2011variation}, \citet{dell2011systematic}, \citet{pawar2016thermal}). It is well documented and understood that nearly all rates of biological activity (including metabolic rates and biochemical reaction rates) increase exponentially with temperature up to some optimum, \textit{$T\textsubscript{pk}$}, then rapidly decline with a further temperature increase (\citet{schoolfield1981non}, \citet{brown2004toward}, \citet{huey1984jack}, \citet{huey1979integrating}, \citet{huey1989evolution}). This initial increase in activity with increase in temperature is described by the Boltzmann factor from the Van't Hoff-Arrhenius equation (\citet{boltzmann66weitere}, \citet{arrhenius1889reaktionsgeschwindigkeit}):
	
	\begin{equation}
	e^{\frac{-E}{kT}}
	\end{equation}
	
	It has been proposed that the thermal response of a trait measured within the temperature range below \textit{$T\textsubscript{pk}$} can, in general, be described mechanistically using the Boltzmann-Arrhenius (BA) model of biochemical kinetics (\citet{brown2004toward}, \citet{gillooly2001effects}, \citet{savage2004predominance}, \citet{pawar2016thermal}):
	
	\begin{equation}
	B = B\textsubscript{0}e^{\frac{-E}{kT}}
	\end{equation}
	
	The decrease in thermal performance occurring in the temperature range above \textit{$T\textsubscript{pk}$} is linked to these higher temperatures causing a denaturation of critical proteins (\citet{knies2010erroneous}, \citet{sharpe1977reaction}, \citet{schoolfield1981non}). A thermal response measured across a broader range of temperatures capturing this rapid decline after the inflection at \textit{$T\textsubscript{pk}$} is considered to be better described using an alternative mechanistic model: the Sharpe-Schoolfield (SS) model (\citet{schoolfield1981non}). 
	
	\begin{equation}
	B =\frac{{B\textsubscript{0}}e^{\frac{-E}{k}(\frac{1}{T}-\frac{1}{283.15})}}{1+e^{\frac{E\textsubscript{l}}{k}(\frac{1}{T\textsubscript{l}}-\frac{1}{T})}+e^{\frac{E\textsubscript{h}}{k}(\frac{1}{T\textsubscript{h}}-\frac{1}{T})}}
	\end{equation}
	
	It is well researched and widely accepted that instantaneous plant metabolism (both respiration and photosynthesis) responds predictably to changes in temperature according to the mechanistic models described above (\citet{hew1969effects}, \citet{brown2004toward}, \citet{yvon2012reconciling}, \citet{price2010metabolic}). Both the BA and SS models aim to capture the physiological mechanisms behind the thermal response of a trait, but at an instantaneous time scale. It is therefore reasonable to ask, will these mechanistic models be less successful in fitting data across longer time scales? 
	Specifically, is there an observable difference in the fit of a phenomenological model vs. one of these mechanistic models at day, week and month time scales?
	
	More recently, a number of studies have demonstrated the ability of respiration and photosynthesis to significantly acclimate their responses to an increase in temperature (\citet{reich2016boreal}, \citet{xiong2000photosynthetic}, \citet{gunderson2000acclimation}). Research also shows that the incorporation of this acclimation in climate change prediction models gives significantly different results compared to ones that ignore it (\citet{lombardozzi2015temperature}, \citet{smith2013plant}). However, the inclusion of acclimation in improving model fit on currently available data is less well understood, and there is no widespread acceptance on how it should be incorporated (\citet{smith2015foliar}, \citet{way2014thermal}). I aim to clarify the justification of using these mechanistic models across all time scales without any alterations accounting for acclimation and to investigate what might be implemented in order to improve these models when used across different time ranges.
	
	\section{Data and methods}
	%write the proposed methods here
	I used eddy covariance data from the FLUXNET database to fit the varying time scale models (http://fluxnet.ornl.gov, (ORNL \citet{fluxnet}). I extracted the nighttime observations with the intention of extracting respiration-only data, since the night is a guaranteed period of photosynthetic-inactivity. By limiting my study to respiration data, I remove the complexity that comes with investigating the interaction between photosynthesis, respiration and their potentially differing degrees of acclimation. The insights from a study on respiration will only help in deconstructing the more elaborate dynamics that are present in the acclimation of net ecosystem exchange (NEE) during the day.
	
	Half-hourly measures of temperature in Celsius (\textit{TA\_F}), incoming photosynthetic photon flux density (\textit{PPFG\_IN}), net ecosystem exchange (\textit{NEE} with a positive value indicating overall release of CO\textsubscript{2}), and nighttime flag (\textit{NIGHT}) were used in analyzing the data. I removed freezing temperature data to filter for growth period and discarded data with anomalous low GPP and respiration ($<0.1$) since these very small values could be within the margin of error for an actual 0 value reading. I used nighttime observations where \textit{NEE} was $>0$ since a binary nighttime flag results in an unclear value assigned to the transition period of night and day. During this time when there is some light available and photosynthesis is occurring, the photosynthetic activity can outweigh respiration resulting in \textit{NEE} $<0$: a counter-intuitive value for the nighttime, which is meant to screen for respiration-only data. Lastly, I removed interpolated data for \textit{NEE} values and kept only those where the quality flag was either good quality gap fill or measured data.
	
	After this filtering, I removed the sites by year covering a temperature range of less than 5$^{\circ}$C and including less than 100 data points. Ultimately, 131 sites consisting of 929 site-years of data were included in my study. The sites cover a range of 11 terrestrial ecosystem types: deciduous broadleaf forests (\textit{DBF}), savannas (\textit{SAV}), grasslands (\textit{GRA}), evergreen needleleaf forests (\textit{ENF}), open shrublands (\textit{OSH}), croplands (\textit{CRO}), permanent wetlands (\textit{WET}), woody savannas (\textit{WSA}), mixed forests (\textit{MF}), closed shrublands (\textit{CSH}), evergreen broadleaf forests (\textit{EBF}). 
	
	Since freezing temperatures were removed from the data, it is likely that low temperature inactivation is largely absent from the observations. The simplified Schoolfield (SSH) model (\citet{schoolfield1981non}) with the low temperature enzyme inactivation effect removed is thus a better suited mechanistic model for this data and was used instead of the Sharpe-Schoolfield model (equation 2). This simplified model is achieved by removing the second exponential term in the denominator of equation 2, giving a four parameter model: 
	
	\begin{equation}
	B =\frac{{B\textsubscript{0}}e^{\frac{-E}{k}(\frac{1}{T}-\frac{1}{283.15})}}{1+e^{\frac{E\textsubscript{h}}{k}(\frac{1}{T\textsubscript{h}}-\frac{1}{T})}}
	\end{equation}
	
	I subset the observations by ecosystem type and used these sets to explore BA and SSH model fitting across time scales from daily to yearly data. %Do i really need to add this (below)?? 
	%In grouping the data by ecosystem type instead of site ID, the observation of emerging trends would be relevant in areas of the same ecosystem type for which FLUXNET data is not available. These results would thus be more beneficial for climate change impact forecasts as they would be based on a more general grouping criteria and could be applicable on a much broader scale than simply the sites for which we already have information. 
	Single day data sets for each ecosystem type were taken in increments of 28 days. Single week data sets were taken as the range of data from the single day plus the six days following it. A month was designated a period of 28 days (4 weeks) and single month data sets were taken as the range of data from the single day plus the 27 days following it. Non-linear least square (NLLS) fitting was performed on each of the data sets using BA, SSH and a second-degree polynomial. These NLLS fitting results within a single data set were then compared using Bayesian information criterion (BIC) and a model of best fit was chosen. Ultimately, I ended up with 4485 sets of data with NLLS fitting and model selection results. 
	
	\section{Results}
	
	At each temporal scale within a single ecosystem type, I took the counts of BA, SSH and second-degree polynomial models being selected as the best fitting model (sample plotted model fits are shown in the supplementary information (S1)). The SSH model was not the best fitting model in any of the data sets so I compared only the results of the BA model with the second-degree polynomial. I ran chi-squared (${\chi}^2$) tests on each ecosystem type to determine if the temporal scale of the observations was independent of the model selection (either phenomenological or mechanistic). The results are shown in Table 1. There were no significant results indicating a relationship between time scale (day, week or month) and model selection (mechanistic: BA, phenomenological: second-degree polynomial) in any of the ecosystem types.
	
	\afterpage{
		\import{Figures/}{chisq_table.txt}
		\begin{figure}[H]
			\caption*{{\bfseries Table 1} Chi-squared test results for determining the existence of a relationship between time scale (day, week or month) and model selection (mechanistic: BA, phenomenological: second-degree polynomial) in each ecosystem type. The results show that no significant relationship was found for any of the ecosystems.}
		\end{figure}
		
		
		\begin{figure}[H]
			\centering
			\includegraphics[width = 175mm, scale = 0.5]{model_fits_table.pdf}
			\caption*{{\bfseries Figure 1}: Proportion of phenomenological (2nd deg) model selection in each of the ecosystem types at the day, week and month time scales. The second-degree polynomial is seen clearly as the best fitting across the board. For the data with a best fit second-degree polynomial, I calculated the percentage that were significantly better fit by the second-degree polynomial compared to the BA model (F test, p$<$0.05). These proportions including only significantly better fits are illustrated as the area below the black lines. It can be seen that even with the exclusion of non-significant results, the second-degree polynomial still covers a larger proportion than the red-filled Boltzmann-Arrhenius.}
		\end{figure}	
	}
	
	In Figure 1, we see the proportion of BA versus second-degree polynomial best fit selections at each time scale and ecosystem type. It is clear that the second-degree polynomial was the predominantly best fitting model across all scales and ecosystem types although, as I found before, there is no significant difference in this predominance across temporal scale. In each of the data sets where the second-degree polynomial was chosen, the second-best fitting model was consistently the BA model (had the next-lowest BIC value). This lead me to investigate where given that the second-degree polynomial was chosen, how often was it \textit{significantly} better fitting than the BA model (F test, p$<$0.05) and would the exclusion of these non-significant better fits change any results. This is also shown in Figure 1 and it can be seen that including only the proportion of significantly better fitting second-degree polynomials would not result in any of the entries changing to the BA model being a superior fit.
	
	In order to explore what changes might be implemented to improve the mechanistic models across time ranges, I looked into whether there were any significant differences in NLLS fitted parameter values between the different time scales in each ecosystem type. In particular, I evaluated the differences in mean values of $E$, $E\textsubscript{h}$, and $B\textsubscript{0}$ at the day, week, and month time scales for the two mechanistic models using one-way ANOVA tests (see descriptions and results in the supplementary information (S2)). The results showed no significant differences between the mean parameter values measured at increasing time scales, with the exception of CRO and EBF $E$ values measured in the simplified Schoolfield model ((\textit{F}(2, 263)=4.565, \textit{p}=0.01), and (\textit{F}(2, 178)=3.07, \textit{p}=0.049), respectively).  
	
	\section{Discussion}
	Overall, there were no significant differences in the model fits across time scales as would be expected with evidence of acclimation in the data. These results are consistent with those found by Smith et al. (2015) where they observed no significant improvements to NEE model fitting by including acclimation in respiration alone. It is important to note that I was not looking at overall NEE or photosynthesis, for which including acclimation has been shown to significantly improve model fitting (\citet{smith2015foliar}). It would therefore be of interest to use the FLUXNET data but include both nighttime and daytime data in the analysis as this might lead to similar results to Smith et al. (2015) in observing notable acclimation from photosynthesis and overall NEE.
	
	There has been significant research in the sensitivity of the Boltzmann-Arrhenius equation across different experimental temperature ranges (\citet{knies2010erroneous}, \citet{pawar2016thermal}, \citet{ratkowsky1982relationship}). The results from these studies demonstrate the potential limitations of or difficulty in appropriately applying the Arrhenius equation to temperature-dependent trait data.  Specifically, Knies and Kingsolver (2010) showed that if the maximum temperatures included in the experimental range were near to the $T\textsubscript{pk}$, then the data shown on a Boltzmann-Arrhenius plot might demonstrate non-linearity (specifically, a single curvature). This is because the Arrhenius-based model is a purely probabilistic description of reaction kinetics, so towards the upper values of a BA plotted line, near $T\textsubscript{pk}$, you will expect an increase in the variation of the trait value and thus, the trait value itself may no longer increase strictly exponentially and this curvature can be generated. This results in significantly better fitting second-degree polynomials on Arrhenius-plotted data where the upper cutoff of the temperature range is at or very near to $T\textsubscript{pk}$ (\citet{knies2010erroneous}). The temperature readings included in the data are potentially in this range of temperatures with a cutoff value very near the measured respiration's $T\textsubscript{pk}$. It is possible that among the nighttime temperatures measured between 1991 and 2014, values considerably beyond the $T\textsubscript{pk}$ for respiration were not experienced so a negligible number of observations for these temperature ranges are recorded in the data sets. This could help to explain the predominant best-fit of the second-degree polynomial across the data sets in both ecosystem types and temporal scales. 
	Furthermore, if there were limited observations in the range of temperatures beyond $T\textsubscript{pk}$, this would mean that the rapid decline captured by the simplified Schoolfield model would not be critical to a significantly better fit of the data. The SSH is penalized in its BIC score for having more parameter values than the second-degree polynomial or the BA model, and here, this cost outweighs the advantages it provides in fitting this high temperature decline.
	
	It has been proposed that respiratory maintenance, or short-term acclimation, can result in a decline of the $Q\textsubscript{10}$ value: a result that would be similar to a change in $E$ (\citet{tjoelker2001modelling}). Another prominent opinion in the literature is that acclimation can be captured by a change in $B\textsubscript{0}$ corresponding to a shift in the $T\textsubscript{pk}$ with other parameter values more or less maintained (\citet{way2014thermal}, \citet{pawar2015metabolic}). Contrary to what might have been expected in $B\textsubscript{0}$ or $E$, I found no significant trends in the parameter calues across time scales and thus, no obvious alteration to implement in these mechanistic models as a simple way of improving performance. More advanced algorithms have also been proposed to simulate acclimation of plant respiration to temperature (\citet{wythers2005foliar}, \citet{king2006atmosphere}, \citet{atkin2008using}, \citet{ziehn2011improving}) and are applicable across a range of model scales (from ecosystem to global level). These more complex algorithms provide evidence that capturing the trends of acclimation in simple changes of parameter values may be difficult and unsuccessful so it is perhaps unsurprising that I found no straightforward parameter differences across time scales. 
	
	\section{Conclusion}
	
	Investigating thermal acclimation of plant respiration and overall NEE will improve our ability to model carbon flux in both current and future data. This study demonstrates the difficulties that are faced when investigating the effects of climate change on biological processes.  Acclimation has been claimed a significant driver in our planet's reduction of atmospheric carbon abundance. However, the limits of acclimation are still not well understood and the long-term implications of climate change on plant metabolism are unfavourable. Results often show that predictions are not as straightforward as we would hope but they also illustrate the necessity for better understanding of carbon cycling and how it might be affected by rapidly increasing temperatures.
	
	\clearpage
	%references go here 
	\bibliographystyle{apa}
	\bibliography{Full_Biblio.bib}
	
	\clearpage
	\section{Supplementary Information}
	\textbf{S1}: 
	A set of sample plotted NLLS model fits for each ecosystem type. From left to right, you see the day, week and month time scales for a set of data from a single ecosystem type. Shown here are just 11 of the 1243 multiple time scale plots.
	
	\begin{figure}[H]
		\caption*{{\bfseries Croplands (CRO) NLLS fitting example}}
		\centering
		\includegraphics[width = 175mm, scale = 0.5]{CRO28.png}
	\end{figure}
	
	\begin{figure}[H]
		\caption*{{\bfseries Closed Shrublands (CSH) NLLS fitting example}}
		\centering
		\includegraphics[width = 175mm, scale = 0.5]{CSH496.png}
	\end{figure}
	
	\begin{figure}[H]
		\caption*{{\bfseries Deciduous Broadleaf Forest (DBF) NLLS fitting example}}
		\centering
		\includegraphics[width = 175mm, scale = 0.5]{DBF8.png}
	\end{figure}
	
	\begin{figure}[H]
		\caption*{{\bfseries Evergreen Broadleaf Forest (EBF) NLLS fitting example}}
		\centering
		\includegraphics[width = 175mm, scale = 0.5]{EBF4.png}
	\end{figure}
	
	\begin{figure}[H]
		\caption*{{\bfseries Evergreen Needleleaf Forest (ENF) NLLS fitting example}}
		\centering
		\includegraphics[width = 175mm, scale = 0.5]{ENF12.png}
	\end{figure}
	
	\begin{figure}[H]
		\caption*{{\bfseries Grassland (GRA) NLLS fitting example}}
		\centering
		\includegraphics[width = 175mm, scale = 0.5]{GRA16.png}
	\end{figure}
	
	\begin{figure}[H]
		\caption*{{\bfseries Mixed Forest (MF) NLLS fitting example}}
		\centering
		\includegraphics[width = 175mm, scale = 0.5]{MF8.png}
	\end{figure}
	
	\begin{figure}[H]
		\caption*{{\bfseries Open Shrublands (OSH) NLLS fitting example}}
		\centering
		\includegraphics[width = 175mm, scale = 0.5]{OSH8.png}
	\end{figure}
	
	\begin{figure}[H]
		\caption*{{\bfseries Savanna (SAV) NLLS fitting example}}
		\centering
		\includegraphics[width = 175mm, scale = 0.5]{SAV76.png}
	\end{figure}
	
	\begin{figure}[H]
		\centering
		\caption*{{\bfseries Permanent Wetland (WET) NLLS fitting example}}
		\includegraphics[width = 175mm, scale = 0.5]{WET48.png}
	\end{figure}
	
	\begin{figure}[H]
		\caption*{{\bfseries Woody Savanna (WSA) NLLS fitting example}}
		\centering
		\includegraphics[width = 175mm, scale = 0.5]{WSA356.png}
	\end{figure}
	
	\textbf{S2}:
	One-way ANOVA summary tables analyzing the difference in parameter values across time scales within an ecosystem type (in both Boltzmann-Arrhenius and simplified Sharpe-Schoolfield fitted models). 
	
	\import{Figures/}{CRO_parameters.txt}
	
	\import{Figures/}{CSH_parameters.txt}
	
	\import{Figures/}{DBF_parameters.txt}
	
	\import{Figures/}{EBF_parameters.txt}
	
	\import{Figures/}{ENF_parameters.txt}
	
	\import{Figures/}{GRA_parameters.txt}
	
	\import{Figures/}{MF_parameters.txt}
	
	\import{Figures/}{OSH_parameters.txt}
	
	\import{Figures/}{SAV_parameters.txt}
	
	\import{Figures/}{WET_parameters.txt}
	
	\import{Figures/}{WSA_parameters.txt}
	
	
\end{document}
