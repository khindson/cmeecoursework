% latex table generated in R 3.2.3 by xtable 1.8-2 package
% Wed Feb 15 11:19:00 2017
\begin{table}[ht]
\centering
\caption*{{\bfseries Evergreen Broadleaf Forest (EBF) parameter ANOVA results}}
\begin{tabular}{lllrr}
  \hline
Parameter & Degrees of Freedom & Sample Size (N) & F-value & p-value \\ 
  \hline
  B0 Boltzmann & 2 & 479 & 1.258 & 0.29 \\ 
  E Boltzmann & 2 & 479 & 0.082 & 0.92 \\ 
  B0 Schoolfield & 1 & 111 & 2.266 & 0.14 \\ 
  E Schoolfield & 1 & 111 & 3.07 & 0.05* \\
  Eh Schoolfield & 1 & 111 & 0.218 & 0.64 \\ 
  \hline
\end{tabular}
\end{table}
