% latex table generated in R 3.2.3 by xtable 1.8-2 package
% Wed Feb 15 11:19:00 2017
\begin{table}[ht]
\centering
\caption*{{\bfseries Closed Shrublands (CSH) parameter ANOVA results}}
\begin{tabular}{lllrr}
  \hline
Parameter & Degrees of Freedom & Sample Size (N) & F-value & p-value \\ 
  \hline
  B0 Boltzmann & 2 & 143 & 0.892 & 0.41 \\ 
  E Boltzmann & 2 & 143 & 1.643 & 0.20 \\ 
  B0 Schoolfield & 1 & 16 & 0.596 & 0.45 \\ 
  E Schoolfield & 1 & 16 & 1.118 & 0.34 \\
  Eh Schoolfield & 1 & 16 & 1.749 & 0.21 \\ 
  \hline
\end{tabular}
\end{table}
