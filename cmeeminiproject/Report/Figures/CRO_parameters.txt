% latex table generated in R 3.2.3 by xtable 1.8-2 package
% Wed Feb 15 11:19:00 2017
\begin{table}[ht]
\centering
\caption*{{\bfseries Croplands (CRO) parameter ANOVA results}}
\begin{tabular}{lllrr}
  \hline
Parameter & Degrees of Freedom & Sample Size (N) & F-value & p-value \\ 
  \hline
  B0 Boltzmann & 2 & 429 & 0.672 & 0.51 \\ 
  E Boltzmann & 2 & 429 & 1.612 & 0.20 \\ 
  B0 Schoolfield & 1 & 266 & 0.002 & 0.96 \\ 
  E Schoolfield & 1 & 266 & 4.565 & 0.01* \\
  Eh Schoolfield & 1 & 266 & 1.649 & 0.19 \\ 
  \hline
\end{tabular}
\end{table}
