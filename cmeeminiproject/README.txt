				    cmeeminiproject

This directory contains the Code, Data, Results and Report sub-directories for 
cmeeminiproject. My miniproject aimed to test the effects of temporal scale on 
respiration thermal performance curve-fitting using the FLUXNET data set. 

CODE DEPENDENCIES:
--> R (developed using version 3.2.3)
--> Python (developed using version 2.7.12)
--> R packages: xtable, graphics, ggrplot2, grid, gridExtra, formattable, plyr
		reshape2, base  
--> perl (developed using version 5.22.1)...required for TeXcount
--> TeXcount (version 3.0.0.24)...use: sudo apt install texlive-extra-utils
    the texcount.pl script is already included in the Report subdirectory

Please note that I have NOT included any of the actual data and results (with the 
exception of the fitted NLLS plots and the report itself) since these files would
exceed 50GB combined (not something you want on BitBucket...). If you would like 
any of the data or results, please let me know, and I can get it to you some other 
way. 

I have created a run_miniproject.py script that will run through the entire process
of the miniproject using some sample data and will feed the results into a sample 
report. This sample report is called CMEE_miniproject_Khindson_sample.pdf the  called actual report is CMEE_miniproject_KHindson.pdf and can be found in the Report directory. 
The sample report will have the same results as the actual report, and this is 
because the Chi-Squared test fails when it's run on the sample report because there
is not enough data for it to provide accurate estimations (so it throws an error). 
So, the actual results are fed to the sample report in order for results tables to 
be built without error. 

The code is all set up to run through the sample data, but the code needed to run 
through the actual data is simply hashed out, so it's easy to see where you would 
just need to load a different file name or data set in order to run through the 
actual results. 

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

See below for the list and brief descriptions of each file contained in 
the sub-directories.

List of contents in the Code sub-directory:
==========================================

data_cleanup_simple.R
----------------------

This is cleaning the data from the raw full data set to remove low quality 
observations, only growth period, and only sites with a number of data 
points above a certain threshold.

ecosystemtype_added.R
----------------------

This adds the siteID ecosystem type information and the temperature
in Kelvin to the clean data set (either fluxnet_clean or sample_data_clean)
and returns a new .Rdata file (either fluxnet_clean_ecoK or 
sample_data_clean_ecoK).

split_night_day.R
------------------

This parses & subsets the ecoK data into nighttime data and daytime data. It
returns two .Rdata files: one containing night data, and one containing day data
(FitDataNightHH & FitDataDayHH or sample_dataDay & sample_dataNight).

subset_data.R
---------------

Subsets the night time data by ecosystem type, then takes this data and 
further subsets it into day, week and month time scale datasets. It 
saves all of these into the respective folders in the Data directory. 

NLLS_do.R
----------

This performs the NLLS on the data, plots different fit lines, and compares 
the fit of the models using BIC. The results are then saved in the appropriate
directory within the Results directory. 

extract_values_NLLS.R
----------------------

This runs through the NLLS results and extracts the values and runs chi-squared
and ANOVA tests to investigate the proposed hypotheses. It saves the resulting 
files in a directory in the Results directory. 

graph_plots.R
--------------

This graphs plots across a varying time scale for a single ecosystem type onto 
a single plot and saves it as a multiplot to a .pdf file. 

make_tables.R
-------------

This builds the tables to be added to the LaTeX file at the end 
NOTE: you are required to have installed the xtable package as this
is used for building the tables

run_miniproject.py
------------------

This is the code that runs the sample data through the entire miniproject
and produces the final results. 

List of contents in the Data sub-directory:
===========================================

sample_data.Rdata
-----------------

This is the sample data file that is simply a subset of the first 60 000 
observations from the FLUXNET data set. This is used to run through the 
entire mini project, as the actual data file would take more than a day to 
run through. 

SingleDayData
--------------

Data split into single day data sets for each ecosystem type. 

SingleWeekData
--------------

Data split into single week data sets for each ecosystem type. 

SingleMonthData
--------------

Data split into single month data sets for each ecosystem type. 

SplitByEcotypes
---------------

Data split into each ecosystem type and saved as a .rda file. 

List of contents in the Results sub-directory:
==============================================

Extracted_NLLS_vals
-------------------

These are the values extracted from the NLLS plotting results. For each 
ecosystem type, there is a pdf with the boxplots for each of the parameter 
value distributions (to make sure they're normally distributed for running 
ANOVA tests), a .rda file containing all of the parameter value results, a 
.txt file with all of the results from the ANOVA tests and their summary 
tables, and a .txt file with the ChiSq summary results table. 

Model_Selection_vals
---------------------

These are the values extracted from the model selection results in the NLLS 
fitting. They are seeing which are the models of best fit, and running 
F-tests to see if the fits are significantly better than the alternative models.

Multiplots
----------

These are the .pdf files which contain the multi-panneled plots for a given 
temporal scale set with the plotted NLLS fittings and data points. 

Multiplots_Failed
-----------------

This contains an output file that is made when there is not a full set (day, 
week and month) for a given ecosystem type subset, so a multiplot was not 
constructed. 

NLLS_Errors
-----------

These are the error files created when there is a problem with the NLLS fitting. 
The error message is printed in the file and the data being fitted at the time 
is the file name. 

NLLS_Results
------------

Thesea are the results files for the NLLS fitting. 

List of contents in the Report sub-directory:
=============================================

Figures
--------

These are the figures included in the report. 

CMEE_miniproject_KHindson.pdf, .tex, .bbl
------------------------------------------

This is the actual report. 

CMEE_miniproject_KHindson_sample.pdf, .tex, .bbl
-------------------------------------------------

This is the report produced from the sample data.

CompileLaTeX_sample.sh & CompileLaTeX.sh
----------------------------------------

A LaTeX compiling program that first removes the file extension (if any) 
of the input argument filename then compiles it to a pdf file and its 
corresponding bibliography. 

NOTE: if inputting a filename that has a '.' character but no extension, 
the code will remove the characters after the '.' and the program won't 
work as designed. Ex. filename.test == filename

word_count_sample.sh & word_count.sh
-------------------------------------

This is the bash code that produces a word count file and then this is 
fed to the LaTeX report. 

Full_Biblio.bib
---------------

The bibliography file for the report. 

all .sty documents
------------------

These are packages used in the LaTeX report, depending on the LaTeX compiler
you have, they may or may not be downloaded, so they're provided here in case.

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


