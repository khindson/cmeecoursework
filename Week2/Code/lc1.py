#!usr/bin/python

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 

# (2) Now do the same using conventional loops (you can shoose to do this 
# before 1 !). 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

"""This code takes a list of birds and separately outputs lists of latin names, common names and body masses using both list comprehensions and loops."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'


#this is a list comprehension that lists the latin names of the birds
 
latin_names_lc = [[i[0] for i in birds]]
print "These are the Latin names: "
print latin_names_lc


#this is a list comprehension that lists the common names of the birds

common_names_lc = [[i[1] for i in birds]] 
print "These are the common names: "
print common_names_lc


#this is a list comprehension that lists the mean body masses of the birds

mean_body_mass_lc = [[i[2] for i in birds]]
print "These are the mean body masses: "
print mean_body_mass_lc


#this is a loop that lists the latin names of the birds

latin_names_loop = [] # creates an empty list
for i in birds:
	latin_names_loop.append(i[0]) # adds the 0th element of each entry in the list, birds, and adds this to the empty list latin_names_loop
print "These are the Latin names: "
print latin_names_loop


#this is a loop that lists the common names of the birds

common_names_loop = [] 
for i in birds: 
	common_names_loop.append(i[1])
print "These are the common names: "
print common_names_loop


#this is a loop that lists the mean body masses of the birds

mean_body_mass_loop = [] 
for i in birds:
	mean_body_mass_loop.append(i[2])
print "These are the mean body masses: "
print mean_body_mass_loop
	


