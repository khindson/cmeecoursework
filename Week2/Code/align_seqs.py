#!usr/bin/python

"""This module contains functions that are used for DNA sequence alignment calculations. There are functions used to convert a .csv file containing DNA sequences into output that can then be used by the other module's functions to calculate sequence alignment results."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import csv
import sys

def csv_get_seqs_func(csv_filename):
	
	"""Takes a single .csv file as input formatted to contain two DNA sequences as two comma separated values and returns the two sequences separated within a tuple."""

	data = csv.reader(open(csv_filename, 'rb'))
	for row in data:
		csv_seq1 = row[0] # assign seq1 to be the first entry in the csv file
		csv_seq2 = row[1] # assign seq 2 to be the second entry in the csv file
	return (csv_seq1, csv_seq2) # this returns a tuple...so it's seen as a single variable, which is why we need to unpack it later to be used in align_seqs_func
	csv_filename.close()
	
def align_seqs_func(seq1, seq2):
	
	"""This is a function composed of sub functions which, when run, finds the best alignment of two input DNA sequences, returns the best alignment results and records them in the bestalignment.txt file."""
		
	#this assigns the longest sequence of the two input sequences to the variable, s1 with length, l1 and the shortest sequence of the two input sequences to the variable, s2 with length, l2
	
	l1 = len(seq1)
	l2 = len(seq2)
	if l1 >= l2:
		s1 = seq1
		s2 = seq2
	else:
		s1 = seq2
		s2 = seq1
		l1, l2 = l2, l1 # swap the two lengths

	def calculate_score(s1, s2, l1, l2, startpoint):
		
		"""This function calculates the match score for the number of characters matched in the alignment between the two strings with the shorter of the two being aligned starting at some startpoint integer input. It also prints the resulting match."""
		
		# startpoint is the point at which we want to start
		matched = "" # contains string for alignement...is an empty string that will be filled with match symbols
		score = 0 # initializing a score variable
		for i in range(l2): # for the range of the shorter sequence
			if (i + startpoint) < l1:  # so long as the startpoint is before the last character in the seq1, run this, otherwise, exit
				if s1[i + startpoint] == s2[i]: # if the character that your counter is at is a matching character in the two sequences then:
					matched = matched + "*" # add a value of * to the string 'matched' at that character's position
					score = score + 1 # and add 1 to the score 
				else:
					matched = matched + "-" # otherwise add a value of - to the string 'matched' at that character's position

		# build some formatted output
		
		print "." * startpoint + matched # this prints the '.' character up to the startpoint character value (* prints the preceding string the amount of times specified by the following integer), then prints the 'matched' string after           
		print "." * startpoint + s2 # this prints the '.' character up to the startpoint character value then prints the shorter sequence that is being matched directly under the 'matched' string so you can see which characters were matched
		print s1 # prints the longer sequence directly under the searching, shorter sequence so that you can see which point of the longer sequence it was matched to
		print score 
		print ""

		return (score, matched)

	# now try to find the best match (highest score)
	my_best_align_sequence = None # initializes a value for the best alignment string value
	my_best_align_matches = None
	my_best_score = -1 # initializes a value for the best score value...must be -1 and not 0 because if we get a 0 score as the best alignment, then we want my_best_align to be assigned a value, not just kept empty...this will happen if we set it to be -1, because the lowest score we can get is 0

	for i in range(l1): # for every possible startpoint on s1, run through this loop
		(z, y) = calculate_score(s1, s2, l1, l2, i) # run the calculate_score function for each startpoint along s1
		if z > my_best_score: # if the output score from running the above line is greater than the current highest score being stored, then replace the current alignment being stored with the alignment that is being run through in the current iteration
			my_best_align_matches = "".join(("." * i + y).splitlines())
			# this saves the 'matched' output string information from the calculate_score function for the best alignment match. This means that it can later be printed above the best alignment sequence so that the matching characters' disctribution can be easily visualized. 
			my_best_align_sequence = "." * i + s2
			my_best_score = z


	with open('../Results/bestalignment.txt','a') as bestalignment: # opens nd appends a file with the listed outputs in the Results folder and using 'with' also closes it automatically
		print >> bestalignment, "Best alignment information for the input sequences: "
		print >> bestalignment, my_best_align_matches 
		print >> bestalignment, my_best_align_sequence # print the best alignment of s2 on s1
		print >> bestalignment, s1 
		print >> bestalignment, "Best score:" + str(my_best_score)

def main(argv):
	
	# testing the functions using a default .csv file
			
	align_seqs_func(*csv_get_seqs_func('../Sandbox/sequences.csv')) # runs the sequence alignment on the file 'sequences.csv'
	# the * before the second function call 'unpacks' the tuple returned from csv_get_seqs_func, feeding the align_seqs_func the two variables from the tuple as separate arguments, rather than feeding it just a single tuple argument

	print "Your sequences have been aligned! Please see the results appended to the file called 'bestalignment.txt' in the Results folder."
	

	return 0
	
if (__name__=="__main__") : 
	status = main(sys.argv)
	sys.exit(status)
	
