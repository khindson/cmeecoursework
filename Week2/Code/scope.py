#!usr/bin/python

"""Some examples illustrating the scope of global and local variables."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

## Try this first
_a_global = 10

def a_function():
	
	"""Function used to demonstrate a global vs. local variable."""
	
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the valueis ", _a_local
	return None

a_function()
print "Outside the function, the value is ", _a_global #this prints out 10

## Now try this 

_a_global = 10

def a_function():
	
	"""Function gain used to demonstrate naming conventions and uses of global and local variables."""
	
	global _a_global #only adding this global BEFORE the variable actually changes it to a global variable...the _global is just a naming convention
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global 
	print "Inside the function, the value is ", _a_local
	return None
	
a_function()
print "Outside the function, the value is", _a_global #this prints out 5
