#!usr/bin/python

# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )

# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.

# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !). 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

"""This code takes a list of rainfaill by month and makes lists of months depending on rainfall values specified using both list comprehensions and loops."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'


# this is a list comprehension that makes and prints a list of the months and their corresponding rainfall where the rainfall was >100 mm
 
rain100_month_lc = [[i for i in rainfall if i[1] > 100]]
print "These are the months and the amount of rainfall where there was more than 100mm: "
print rain100_month_lc


# this is a list comprehension that makes and prints a list of the months where the rainfall was <50 mm

rain50_month_lc = [[i[0] for i in rainfall if i[1] < 50]]
print "These are the months where the amount of rainfall was less than 50mm: "
print rain50_month_lc


# this is a loop that makes and prints a list of the months and their corresponding rainfall where the rainfall was >100 mm

rain100_month_loop = []
for i in rainfall:
	if i[1] > 100:
		rain100_month_loop.append(i)
print "These are the months and the amount of rainfall where there was more than 100mm: "
print rain100_month_loop


# this is  loop that makes and prints a list of the months where the rainfall was <50 mm

rain50_month_loop = [] 
for i in rainfall:
	if i[1] < 50:
		rain50_month_loop.append(i[0])
print "These are the months where the amount of rainfall was less than 50mm: "
print rain50_month_loop


