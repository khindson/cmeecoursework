#!usr/bin/python

"""This module demonstrates how to read from and write to .csv files."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import csv

#Read a file containing:
#'Species','Infraorder','Family,'Distribution','Body mass male (Kg)'
f = open('../Sandbox/testcsv.csv','rb')

csvread = csv.reader(f) # assign csvread to the data read from f 
temp = []
for row in csvread: 
	temp.append(tuple(row)) # for every row in the data read, append the row as a tuple to the empty set temp[]
	print row
	print "The species is", row[0]
	
f.close() # must close a file after having opened it 

#write a file containing only species name and Body mass
f = open('../Sandbox/testcsv.csv','rb')
g = open('../Sandbox/bodymass.csv','wb') # opening a file for writing to it

csvread = csv.reader(f)
csvwrite = csv.writer(g)
for row in csvread:
	print row
	csvwrite.writerow([row[0], row[4]]) # write the 1st and 4th entries of each row into the dicument, bodymass.csv
	
f.close()
g.close()

