#!usr/bin/python
"""Description of this program
	you can use several lines"""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

# imports 
import sys # module to interface our program with the operating system

# constants can go here 


#functions can go here 
def main(argv): #argv is a argument variable 
	print 'This is a boilerplate' 
	return 0
	
if (__name__=="__main__") : #make sure the "main" function is called from commandline
	status = main(sys.argv) #sys.argv is an object created by python using the sys module that we imported...this object contains the names of the argument variables in the current scrypt
	#i.e. argv is a function in the sys module that is used to count/carry all of the names of the argument variables in the current script that is running (see sysargv.py)
	sys.exit(status)
