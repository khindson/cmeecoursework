#!/usr/bin/python

"""Used to demonstrate the function of name == main in importing a module vs. running it as the main module."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

# Filename: using_name.py

if __name__ == '__main__': #this command is used so that when you call the program to run by itself, it will run through this 'if' course of code
	#the __main__ is a variable that is reserved for being the first thing that python looks at when it reads a program
	print 'This program is being run by itself'
else:
	print 'I am being imported from another module' #this is the code that is run if using_name.py is NOT called to run on its own (i.e. if it's imported as a module)
