#!usr/bin/python

"""This module contains different short codes that output varying numbers of 'hello's.
It also contains functions that perform short mathematical calculations."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import sys

# 1)
#This prints hello 14 times.
for i in range(3, 17):
	print 'hello'
	
# 2)
#This prints hello 4 times.
for j in range(12): 
	if j % 3 ==0:
		print 'hello'

# 3)
#This prints hello 5 times.
for j in range(15):
	if j % 5 == 3:
		print 'hello'
	elif j % 4 == 3:
		print 'hello'
	
# 4)
#This prints hello 5 times.
z = 0
while z != 15:
	print 'hello'
	z = z + 3

# 5)
#This prints hello 8 times.
z = 12
while z < 100:
	if z == 31:
		for k in range(7):
			print 'hello'
	elif z == 18:
		print 'hello'
	z = z + 1

def foo1(x):
	"""This function calculates the input to the power of 0.5"""
	return x ** 0.5

def foo2(x, y):
	"""This function returns the largest value of the two variables."""
	if x > y:
		return x
	return y

def foo3(x, y, z):
	"""If x is greater than y, this function switches the values of x and y. Also, if the passed on value of y from the previous if command is greater than z, then the values of z and y are switched."""
	if x > y:
		tmp = y
		y = x
		x = tmp
	if y > z:
		tmp = z
		z = y
		y = tmp
	return [x, y, z]

def foo4(x):
	"""This function is calculating a factorial  of x by multiplying starting from 1 up to the input value of x"""
	result = 1
	for i in range(1, x + 1):
		result = result * i
	return result

def foo5(x):
	"""This calculates a factorial of the input variable, x, by multiplying starting from the value of x and going down to the value 1."""
	if x == 1:
		return 1
	return x * foo5(x - 1)


def main(argv):
	
	print foo1(2)
	print foo2(0, 2)
	print foo3(12, 9, 6)
	print foo4(4)
	print foo5(6)
	
	return 0
	
if (__name__=="__main__") : # This is the idiomatic way to tell whether the Python module was executed as a script, or imported from another module. You will only enter the if __name__ == "__main__" block if the file was executed as a script (aka, it is the main module).
	status = main(sys.argv)
	sys.exit(status)
	
