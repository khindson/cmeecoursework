#!usr/bin/python

taxa = [ ('Myotis lucifugus','Chiroptera'),
         ('Gerbillus henleyi','Rodentia',),
         ('Peromyscus crinitus', 'Rodentia'),
         ('Mus domesticus', 'Rodentia'),
         ('Cleithrionomys rutilus', 'Rodentia'),
         ('Microgale dobsoni', 'Afrosoricida'),
         ('Microgale talazaci', 'Afrosoricida'),
         ('Lyacon pictus', 'Carnivora'),
         ('Arctocephalus gazella', 'Carnivora'),
         ('Canis lupus', 'Carnivora'),
        ]

# Write a short python script to populate a dictionary called taxa_dic 
# derived from  taxa so that it maps order names to sets of taxa. 
# E.g. 'Chiroptera' : set(['Myotis lucifugus']) etc. 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

# Write your script here:

"""This code takes a list of taxa and populates a dictionary with order names as keys mapped to the values of sets of taxa of a given taxa list."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

order_names = set()
for i in taxa:
	order_names.add(i[1]) #converting the list into a set...which by definition is a collection with NO DUPLICATES...so this gives me a list with single occurrences of all of the order names present
	
taxa_dict = dict.fromkeys(order_names) #making a dictionary and assigning the order_names to be the keys of the dictionary 

n = len(order_names) # defining an integer variable based on the number of orders
sets = [set() for _ in range(n)] # making 'n' empty sets (where 'n' is the number of orders)


 # each iteration of this loop is a new order name and in each iteration, we search through the taxa list and append a new set (one of the 'n' sets that we made in the previous line) with matching species from each order
 
k = 0 # assigning a counter variable to 0 that will be used in the loop
for i in order_names:
	for j in taxa: 
		if i == j[1]: # for each element in the taxa list, if the element's order name matches the order name for that iteration of the loop, then we add the species name of this element to a set 
			sets[k].add(j[0]) # note, there are 'n' sets, where 'n' is the number of order names...so each order name has its own assigned set
	k = k + 1 # this is a counter so that each time we get to the end of reading through the taxa list, a new set is used to add the species from the next order name being iterated


# each iteration of this loop is a different order name, and in each iteration, we add the set of taxa values to the matching order name keys in the taxa_dict (using the same iterative process as in the last bit of code)

p = 0 # assigning a counter variable to 0 that will be used in the loop
for i in order_names: 
	taxa_dict[i] = sets[p] # this is adding the values from the sets to each of the origin names keys 
	p = p + 1

print str(taxa_dict) # printing the dictionary to make sure that it worked


