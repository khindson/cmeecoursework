#!usr/bin/python
"""This takes a list of tree taxa and searches for oak trees then performs different modifictions to the outputs."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

## Let's find just those taxa that are oak trees from a list of species

taxa = [ 'Quercus robur',
		 'Fraxinus excelsior',
		 'Pinus sylvestris',
		 'Quercus cerris',
		 'Quercus petrae',
		]

def is_an_oak(name) :
	
	"""Looks at the name of an input argument and returns true or false depending on if it starts with 'quercus'."""
	
	return name.lower().startswith('quercus ')
	
oaks_loops = set()
for species in taxa:
	if is_an_oak(species) :
		oaks_loops.add(species)
print oaks_loops

##Using list comprehensions
oaks_lc = set([species for species in taxa if is_an_oak(species)])
print oaks_lc

##Get names in UPPER CASE using for loops
oaks_loops = set()
for species in taxa:
	if is_an_oak(species) :
		oaks_loops.add(species.upper())
print oaks_loops

##Get names in UPPER CASE using list comprehensions
oaks_lc = set([species.upper() for species in taxa if is_an_oak(species)])
print oaks_lc
	
