#!usr/bin/python

"""Short code that was used to demonstrate debugging and how to remove a float division by zero using debugging methods."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

def createbug(x):
	
	"""Using an input argument and dividing it by 0 to demonstrate how to use debugging tools."""
	
	y = x**4
	z = 0.
	y = y/z
	return y
	
createbug(25)
