#!usr/bin/python

"""Some examples illustrating the use of regex elements."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import re

my_string = 'a given string'
#find a space in the string
match = re.search(r'\s', my_string)

print match
#this should print something like 
# <_sre.SRE_Match object at 0x93ecdd30>

# now we can see what has matched 
match.group()

match = re.search(r's\w*', my_string)

# this should return "string"
match.group()

#NOW AN EXAMPLE OF NO MATCH:
# find a digit in the string
match = re.search(r'\d', my_string)

# this should print "None"
print match
	
	# Further Example
MyStr = 'an example'
match = re.search(r'\w*\s', MyStr)

if match:
	print 'found a match:', match.group()
else:
	print 'did not find a match'
	
# Some Basic Examples
match = re.search(r'\d', "it takes 2 to tango")
print match.group() # print 2

match = re.search(r'\s\w*\s', 'once upon a time') # matches the first example of a white space followed by a sequence of characters followed by a whitespace
match.group() # ' upon '

match = re.search(r'\s\w{1,3}\s', 'once upon a time') # matches the first example of a white space, then a set of alphanumeric characters between the length of 1-3 followed by a white space
match.group() # ' a '

match = re.search(r'\s\w*$', 'once upon a time') # matches a white space, followed by a set of characters, followed by the end of the line
match.group() # ' time'

match = re.search(r'\w*\s\d.*\d', 'take 2 grams of H2O') # matches a set of alphanumeric characters, followed by a white space followed by a number followed by any character except a line break (prints 'take 2 ') repeats the any character except  line break until it reaches a number (prints 'grams of H2')
match.group() # 'take 2 grams of H2'

match = re.search(r'^\w*.*\s', 'once upon a time') # matches the beginning of the line then a set of alphanumeric characters (^\w* prints 'once') followed by any character except a line break (^\w*.* would print 'once upon a time') followed by a white space (the \s at the end makes it stop after the last white space, so it must stop at 'a ')
match.group() # 'once upon a 

## NOTE THAT *, +, and {  } are all "greedy":
## They repeat the previous regex token as many times as possible
## As a result, they may match more text than you want
## To make it non-greedy, use ?:

match = re.search(r'^\w*.*?\s', 'once upon a time') #matches the beginning of the line, then a set of alphanumeric characters, followed by any character except a line break (^\w*.* prints 'once upon a time') but with the added '?' it matches the shortest possible version of this pattern, followed by a whitespace character, so it prints 'once '
match.group() # 'once '

## To further illustrate greediness, let's try matching an HTML tag:

match = re.search(r'<.+>', 'This is a <EM>first</EM> test') # with the + it will match the longest possible version of this pattern
match.group() # '<EM>first</EM>'

## But we didn't want this: we wanted just <EM>
## It's because + is greedy!

## Instead, we can make + "lazy"!

match = re.search(r'<.+?>', 'This is a <EM>first</EM> test') # with the ? before the > it will match the shortest possible version of this pattern found
match.group() # '<EM>'

## OK, moving on from greed and laziness

match = re.search(r'\d*\.?\d*','1432.75+60.22i') #note "\" before "."...this \. means find the literal version of this character, not 'match any character except line break'
# matches a set of numbers followed by a '.' with the shortest possible version of this pattern (without the ? it would print the entire thing because it would match up to the second '.'), followed by a set of numbers characters
match.group() # '1432.75'

match = re.search(r'\d*\.?\d*','1432+60.22i') # matches a set of numberscharacters, followed by a '.', but since '+' is not a number, this just matches '1432' and stops there 

##!!!WHY DOES THIS NOT MATCH NONE???
match.group() # '1432'

match = re.search(r'[AGTC]+', 'the sequence ATTCGT') # matches any of the characters inside the [AGTC] group as many times as possible
match.group() # 'ATTCGT'

re.search(r'\s+[A-Z]{1}\w+\s\w+', 'The bird-shit frog''s name is Theloderma asper').group() # ' Theloderma asper'
# matches a whitepsace with any set of capital alphabetical charcters followed by an 'l' followed by a set of alphanumeric characters followed by a whitespace followed by a set of alphanumeric characters 

## NOTE THAT I DIRECTLY RETURNED THE RESULT BY APPENDING .group()
