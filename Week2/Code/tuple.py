#!usr/bin/python

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
        )

# Birds is a tuple of tuples of length three: latin name, common name, mass.
# write a (short) script to print these on a separate line for each species
# Hints: use the "print" command! You can use list comprehension!


# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

"""This code takes a tuple of tuples of bird information and prints this information in two ways: 
	1) with all of the information for one species on a line, and each species on a separate line
	2) with each piece of information for each species on a separate line
	
I was unclear of which one was the sought for answer..."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

for i in birds: #prints out each piece of information for each species on a separate line
	print i[0]
	print i[1]
	print i[2]
	
for i in birds: # prints out the complete information for each species on a separate line
	print i
	
	
