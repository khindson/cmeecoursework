Code
----

Contains all of the code files written to be evaluated during Week2

Data
----

Contains all of the data files used as needed for running code scripts.

Sandbox
-------

Contins files used for experimenting either with output or scripts. Not 
to be mistaken for important and useful scripting masterpieces.

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 

See below for the list and brief descriptions of each file contained in 
the sub-directories ordered alphabetically. 

List of contents in the Code sub-directory:
===========================================

align_seqs.py
-------------

This module contains functions that are used for DNA sequence alignment
calculations. There are functions used to convert a .csv file containing 
DNA sequences into output that can then be used by the other module's 
functions to calculate sequence alignment results

align_seqs_fasta.py
-------------------

This module contains functions that are used for DNA sequence alignment 
calculations. There are functions used to convert .csv, .fasta and .txt 
files containing DNA sequences into output that can then be used by the 
other module's functions to calculate sequence alignment results.

It requires user input to be run as a main module, but there are hashed 
default files that can be used if you would like to run it autonomously. 

Please be sure to read the descriptions of the required formatting of 
input files in the get_seqs_func functions for each of the corresponding 
input file types. The results may be inaccurate if the input files are 
formatted differently from the description.

basic_csv.py
------------

This module demonstrates how to read from and write to .csv files.

basic_io.py
-----------

This is a simple code that demonstrates how to read .txt file input, how 
to save output to a .txt file and how to store objects using 'pickle'.

boilerplate.py
--------------

A simple structural skeleton for a python program. 

cfexercises.py
--------------

This module contains different short codes that output varying numbers of 
'hello's. It also contains functions that perform short mathematical 
calculations.

control_flow.py
---------------

Some functions exemplifying the use of control statements.

debugme.py
----------

Short code that was used to demonstrate debugging and how to remove a float 
division by zero using debugging methods.

dictionary.py
-------------

This code takes a list of taxa and populates a dictionary with order names 
as keys mapped to the values of sets of taxa of a given taxa list.

lc1.py
------

This code takes a list of birds and separately outputs lists of latin names, 
common names and body masses using both list comprehensions and loops.

lc2.py
------

This code takes a list of rainfaill by month and makes lists of months 
depending on rainfall values specified using both list comprehensions and 
loops.

loops.py
--------

This code demonstrates the basic functions of different loop types, including 
infinite loops.

oaks.py
-------

This takes a list of tree taxa and searches for oak trees then performs 
different modifictions to the outputs

regexs.py
---------

Some examples illustrating the use of regex elements.

scope.py
--------

Some examples illustrating the scope of global and local variables.

sysargv.py
----------

This illustrates the use of the function sys.argv

test_control_flow.py
--------------------

Some functions exemplifying the use of control statements in debugging. 

test_oaks.py
------------

Function used to see if an argument is an oak tree or not.

tuple.py
--------

This code takes a tuple of tuples of bird information and prints this 
information in two ways: 
	1) with all of the information for one species on a line, and each 
	   species on a separate line
	2) with each piece of information for each species on a separate 
	   line
I was unclear of which one was the sought for answer...

using_name.py
-------------

Used to demonstrate the function of name == main in importing a module vs. 
running it as the main module.

List of contents in the Data sub-directory:
===========================================

FASTAfiles
----------

Genome sequence data used to test and run the UnixPrac1 bash script. 
Contains 3 fasta files with three genome sequences and two files 
separtely containing the number of AT and GC occurrences in the E.coli
genome. 

JustOaksData.csv
----------------

A .csv file containing the results from test_oaks.py which appends the oak
species found in a .csv file it reads from. 

TestOaksData.csv
----------------

A .csv file containing multiple tree species types used in test_oaks.py.

List of contents in the Sandbox sub-directory:
==============================================

This is my coding Sandbox. In it I play, build my forts and castles, spend 
glorious time...alone...with my computer...and my rubber duck. 

So, you don't need to understand what goes on in here. 

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Contacts
--------

All script contents of this directory have been written by Katie Hindson or 
have been adapted from the CMEE 2016/17 Course Handbook written by Dr. Samraat
Pawar. 

e-mail questions (but obviously Google them first) to: kah15@ic.ac.uk
 







