				    Week4

This directory contains the Code, Data, Sandbox, Handouts, and Lectures sub-directories for 
week 4 (R Week with Stats). This is mainly empty since nothing was required to be saved or
kept as a script within the handouts.  

Code
----

Empty since there were no structured scripts in Week 4.

Data
----

Contains all of the data files used as needed for running code scripts. 

Sandbox
-------

Contains files used for experimenting either with output or scripts. Not 
to be mistaken for important and useful scripting masterpieces.

Handouts
--------

Contains the handouts used in the applied programming part of the lectures. 

Lectures
--------

Contains the lectures used for the week.

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Contacts
--------

All script contents of this directory have been written by Katie Hindson or 
have been adapted from the CMEE 2016/17 Course Handbook written by Dr. Samraat
Pawar. 

e-mail questions (but obviously Google them first) to: kah15@ic.ac.uk



 




	



