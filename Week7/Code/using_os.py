#!usr/bin/python

""" This is using subprocess.os to get a list of files
in different directories."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'


# Use the subprocess.os module to get a list of files and  
# Hint: look in subprocess.os and/or subprocess.os.path and/or 
# subprocess.os.walk for helpful functions

import subprocess

#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

# Type your code here:

# Get the user's home directory.
home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
FilesDirsStartingWithC = []

# Use a for loop to walk through the home directory.
for (dir, subdir, files) in subprocess.os.walk(home):
	for i in dir:
		if i.startswith("C"):
			FilesDirsStartingWithC.append(i)
	for j in files:
		if j.startswith("C"):
			FilesDirsStartingWithC.append(j)

print len(FilesDirsStartingWithC)
#f1 = open('../Sandbox/file1.txt', 'wb')
#for i in range(len(FilesDirsStartingWithC)-1):
	#f1.write(FilesDirsStartingWithC[i])

#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'

# Create a list to store the results.
FilesDirsStartingWithCorc = []

# Type your code here:

for (dirs, subdir, files) in subprocess.os.walk(home):
	for dir in dirs:
		if dir.startswith("C") == True or dir.startswith("c") == True:
			FilesDirsStartingWithCorc.append(dir)
	for file in files:
		if file.startswith("C") == True or file.startswith("c") == True:
			FilesDirsStartingWithCorc.append(file)

print len(FilesDirsStartingWithCorc)
				
#f2 = open('../Sandbox/file2.txt', 'wb')
#for i in range(len(FilesDirsStartingWithCorc)-1):
	#f2.write(FilesDirsStartingWithCorc[i])

#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

HomeFilesDirsStartingWithCorc = []

# Type your code here:

for (dirs, subdir, files) in subprocess.os.walk(home):
	for dir in dirs:
		if dir.startswith("C") or dir.startswith("c"):
			HomeFilesDirsStartingWithCorc.append(dir)
			


