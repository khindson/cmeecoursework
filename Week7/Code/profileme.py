#!usr/bin/python

"""This looks at the use of range vs xrange and how to use profiling 
in python scripts (i.e. how much time each line/command takes)."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

def a_useless_function(x):
	
	"""Looks at using the range function in 100000000 range."""
	
	y = 0
	# eight zeros!
	for i in xrange(100000000):
		y = y + i
	return 0
	
def a_less_useless_function(x):
	
	"""Looks at using the range function in 100000 range."""
	
	y = 0
	#five zeros!
	for i in xqrange(100000):
		y = y + i
	return 0
	
def some_function(x):
	
	"""Runs the functions and prints the value of x to run using profiling."""
	
	print x
	a_useless_function(x)
	a_less_useless_function(x)
	return 0

some_function(1000)


