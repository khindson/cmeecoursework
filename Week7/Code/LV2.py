""" The typical Lotka-Volterra Model simulated using scipy. 
Parameters must be passed to the function in the following order: 
r, a, z, e, K.
Extra credit added where the final prey and predator density are 
printed on the plot if they persist."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import sys

# import matplotlip.pylab as p #Some people might need to do this

def dCR_dt(pops, t=0):
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    
    R = pops[0]
    C = pops[1]
    dRdt = r*R*(1-(R/K)) - a*R*C 
    dCdt = -z*C + e*a*R*C
    
    return sc.array([dRdt, dCdt])

r = float(sys.argv[1])
a = float(sys.argv[2])
z = float(sys.argv[3])
e = float(sys.argv[4])
K = float(sys.argv[5])
	
# Now define time -- integrate from 0 to 15, using 1000 points:
t = sc.linspace(0, 50,  1000)
x0 = 10
y0 = 5 
z0 = sc.array([x0, y0]) # initials conditions: 10 prey and 5 predators per unit area

pops, infodict = integrate.odeint(dCR_dt, z0, t, full_output=True)

infodict['message']     # >>> 'Integration successful.'
prey, predators = pops.T # What's this for?...T means transpose

length = len(prey)-1 # assigning a variable to the length of the predator/prey vectors minus one...it will be used as a locator variable in the vector, so the length-1 is the final element of the vector (since counting starts at 0 but length doesn't)

# if the predator and prey final density are both greater than 0, then print the final densities to the terminal screen
if prey[length] > 0 and predators[length] > 0: 
	print 'The prey final density: '
	print prey[length]
	print 'The predator final density: ' 
	print predators[length]
	
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc=1)
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics')
p.text(25, 2, 'r = {0}, a = {1}, z = {2}, e = {3}\nK = {4}, R_0 = {5}, C_0 = {6}' .format(r, a, z, e, K, x0, y0))
#p.show()
f1.savefig('../Results/prey_and_predators_2.pdf') #Save figure
