#!usr/bin/python

"""This looks at running an R script in python using the 
subprocess module."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import subprocess

subprocess.Popen("Rscript --verbose fmr.R", shell=True).wait()

