#!usr/bin/python

"""This module takes a dictionary input in the form of a text file and
cleans up the data then uses a set of regexes to sort the dictionary data 
into a list of each entry's kingdom, phylum and species."""
	
__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'


import re

# Read the file
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
text = text.decode('ascii', 'ignore')
text = str(text)  # changing from a unicode text to a regular text

# Now write a regular expression my_reg that captures # the Kingdom, 
# Phylum and Species name for each species and prints it out neatly:

my_reg_kingdom = '(?<=Kingdom\s)[A-Za-z]+' # a regex that finds the kingdom type using the lookback 
my_reg_phylum = '(?<=Phylum\s)[A-Za-z]+'
my_reg_species = '(?<=Species\s)[A-Za-z]+\s[A-Za-z]+'

match_kingdom = re.findall(my_reg_kingdom, text)
match_phylum = re.findall(my_reg_phylum, text)
match_species = re.findall(my_reg_species, text)

j = len(match_kingdom) # setting a counter variable to be the length of the number of entries in the kingdom matches list 

listname = ["Kingdom", "Phylum", "Species"]
print(listname)

for i in range(j):
	print(match_kingdom[i], match_phylum[i], match_species[i]) # printing out each entry 

# Hint: you will probably want to use re.findall(my_reg, text)...
# Keep in mind that there are multiple ways to skin this cat! 
