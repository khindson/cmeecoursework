#!usr/bin/python

""" This is running an R code from python."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import subprocess
subprocess.Popen("Rscript --verbose TestR.R > \
../Results/TestR.Rout 2> ../Results/TestR_errFile.Rout", \
shell=True).wait()

# backslashes are used if you want multiline code in python
# verbose prints out everything that runs 
# to run something with arguments, you just add them directly after teh script name 
