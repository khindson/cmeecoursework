#!/bin/bash
#Author: Katie Hindson kah15@ic.ac.uk
#Script: run_LV.sh
#Desc: script that runs the LV1, LV2 and LV3 python scripts and runs profiling on them
#Arguments: none
#Date: Nov 2016

filename1="LV1.py" 
filename2="LV2.py"
filename3="LV3.py"

# running the LV1 nd LV2
ipython $filename1 
ipython $filename2 1. 0.1 1.5 0.75 50
ipython $filename3

# profiling LV1 and LV2
echo -e "%run -p $filename1" | ipython 
echo -e "%run -p $filename2 1. 0.1 1.5 0.75 50" | ipython 
echo -e "%run -p $filename3" | ipython 
