#!usr/bin/python

""" The typical Lotka-Volterra Model simulated using scipy. 
Discrete time function. 
Extra extra credit added."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting


# initials conditions: 10 prey and 5 predators per unit area
R_0 = 10
C_0 = 5 

R_t = R_0
C_t = C_0

predators = []
prey = []

def f(x, r, a, z, e, K, R_t0, C_t0):
	
	"""Function used for looping through iterations of the discrete time
	function and storing the final output as an array of resutls for the 
	predator and prey density values."""
	
	predators =[]
	prey = []
	
	R_t = R_t0
	C_t = C_t0
	
	for i in range(15):
		predators.append(R_t)
		prey.append(C_t)
		
		R_t1 = R_t*(1+r*(1-R_t/K)-a*C_t)
		C_t1 = C_t*(1-z+e*a*R_t)  
    
		R_t = R_t1
		C_t = C_t1
		
		
	return sc.array([predators, prey])
	
# Define parameters:
r = 2.3 # Resource growth rate
a = 0.1 # Consumer search rate (determines consumption rate) 
z = 1.8 # Consumer mortality rate
e = 0.75 # Consumer production efficiency
K = 10 # Prey carrying capacity
t = 15 # time range
pops = f(t, r, a, z, e, K, R_0, C_0)
   
predators = pops[0]
prey = pops[1]

#prey, predators = pops.T # What's this for?...T means transpose
f1 = p.figure() #Open empty figure object
p.plot(range(t), prey, 'g-', label='Resource density') # Plot
p.plot(range(t), predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics')
#p.show()
f1.savefig('../Results/prey_and_predators_3.pdf') #Save figure
