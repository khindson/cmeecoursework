				    Week7

This directory contains the Code, Data, Results and Sandbox sub-directories for 
week 7 (Advanced Biological Computing). 

Code
----

Contains all of the code files written to be evaluated during Week7

Data
----

Contains all of the data files used as needed for running code scripts.

Results
-------

Contains all of the results files produced as output from the code files
during the week. 

Sandbox
-------

Contains files used for experimenting either with output or scripts. Not 
to be mistaken for important and useful scripting masterpieces.

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 

See below for the list and brief descriptions of each file contained in 
the sub-directories ordered alphabetically. 

List of contents in the Code sub-directory:
===========================================

blackbirds.py
-------------

This module takes a dictionary input in the form of a text file and
cleans up the data then uses a set of regexes to sort the dictionary data 
into a list of each entry's kingdom, phylum and species.

DrawFW.py
----------

Plot a snapshot of a food web graph/network.
Needs: Adjacency list of who eats whom (consumer name/id in 1st
column, resource name/id in 2nd column), and list of species
names/ids and properties such as biomass (node abundance), or average
body mass.

fmr.R
------

Plots log(field metabolic rate) against log(body mass) for the Nagy et al 
1999 dataset to a file fmr.pdf.

LV1.py
-------

The typical Lotka-Volterra Model simulated using scipy

LV2.py
------

The typical Lotka-Volterra Model simulated using scipy. Parameters must 
be passed to the function in the following order: 
r, a, z, e, K. 
Extra credit added where the final prey and predator density are 
printed on the plot if they persist.

LV3.py
-------

The typical Lotka-Volterra Model simulated using scipy. 
Discrete time function. Extra extra credit added.

profileme.py
-------------

This looks at the use of range vs xrange and how to use profiling 
in python scripts (i.e. how much time each line/command takes).

run_fmr_R.py
-------------

This looks at running an R script in python using the subprocess module.

run_LV.sh
----------

Script that runs the LV1, LV2 and LV3 python scripts and runs profiling 
on them.


SQLite.py
---------

Accessing, updating and managing SQLite databases with python.

SQLite.R
---------

Demonstrating the use of SQLite in R

TestR.py
--------

This is running an R code from python.

TestR.R
-------

Script used to run R from python.

timeitme.py
-----------

Uses a different type of timing function (timeit) on our module.

using_os.py
------------

This is using subprocess.os to get a list of files in different 
directories.

List of contents in the Data sub-directory:
===========================================

Biotraits.db
-------------

Database of biotrait data and different thermal responses of a variety of 
species types and measured response. 

blackbirds.txt
----------------

A .txt file from which the information was extracted for the blackbirds 
practical on kingdom, phyla, taxa and species for each entry. 

Consumer.csv
----------------

A .csv file containing the information from the biotraits database that 
was only for consumers. 

NagyEtAl1999.csv
----------------

.csv file used in running .fmr and plotting some R code in python. 

Resource.csv
-------------

A .csv file containing the information from the biotraits database that 
was only for resources.

TCP.csv
--------

TCP data again extracted from the biotraits database. 

TraitInfo.csv
--------------

A list of the trait information extracted from the biotraits database. 

List of contents in the Results sub-direcory:
=============================================

fmr_plot.pdf
------------

The plot run in R on the Nagy et al. data using python. 

prey_and_predators_1.pdf
------------------------

The resulting plot from the LV1 code. 

prey_and_predators_2.pdf
------------------------

The resulting plot from the LV2 code. 

prey_and_predators_3.pdf
------------------------

The resulting plot from the LV3 code

TestR.Rout
-----------

The output file from TestR.R run using python

TestR_errFile.Rout
-------------------

The output error file from TestR.R run using python.

List of contents in the Sandbox sub-directory:
==============================================

This is my coding Sandbox. In it I play, build my forts and castles, spend 
glorious time...alone...with my computer...and my rubber duck. 

So, you don't need to understand what goes on in here. 

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Contacts
--------

All script contents of this directory have been written by Katie Hindson or 
have been adapted from the CMEE 2016/17 Course Handbook written by Dr. Samraat
Pawar. 

e-mail questions (but obviously Google them first) to: kah15@ic.ac.uk
 







